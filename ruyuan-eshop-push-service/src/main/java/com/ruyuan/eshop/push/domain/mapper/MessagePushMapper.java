package com.ruyuan.eshop.push.domain.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruyuan.eshop.push.domain.entity.MessagePushDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zhonghuashishan
 */
@Mapper
public interface MessagePushMapper extends BaseMapper<MessagePushDO> {
}
