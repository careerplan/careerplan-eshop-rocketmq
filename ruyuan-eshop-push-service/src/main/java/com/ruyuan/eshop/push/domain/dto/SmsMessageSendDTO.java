package com.ruyuan.eshop.push.domain.dto;

import lombok.Data;

/**
 * @author zhonghuashishan
 */
@Data
public class SmsMessageSendDTO extends MessageSendDTO {

    /**
     * 用户手机号
     */
    private String phoneNum;

}
