package com.ruyuan.eshop.push.mq.consumer;

import com.ruyuan.eshop.push.mq.config.RocketMQProperties;
import com.ruyuan.eshop.push.mq.consumer.listener.*;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.exception.MQClientException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.ruyuan.eshop.common.constants.RocketMqConstant.*;


/**
 * @author zhonghuashishan
 * @version 1.0
 */
@Configuration
public class ConsumerBeanConfig {

    /**
     * 配置内容对象
     */
    @Autowired
    private RocketMQProperties rocketMQProperties;

    /**
     * 平台活动推送消息消费者 completableFuture逻辑
     * @param platFormPromotionListener
     * @return
     * @throws MQClientException
     */
    /*@Bean("platformPromotionSendTopicConsumer")
    public DefaultMQPushConsumer platformPromotionSendConsumer(
            PlatFormPromotionListener platFormPromotionListener) throws MQClientException {
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(PLATFORM_PROMOTION_SEND_CONSUMER_GROUP);
        consumer.setNamesrvAddr(rocketMQProperties.getNameServer());
        consumer.subscribe(PLATFORM_PROMOTION_SEND_TOPIC, "*");
        consumer.registerMessageListener(platFormPromotionListener);
        consumer.start();
        return consumer;
    }
*/
    /**
     * 平台活动推送消息消费者 正常消费逻辑
     * @param platFormPromotionListener
     * @return
     * @throws MQClientException
     */
    @Bean("platformPromotionSendTopicConsumer")
    public DefaultMQPushConsumer platformPromotionSendConsumer(PlatFormPromotionListenerNormal platFormPromotionListener) throws MQClientException {
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(PLATFORM_PROMOTION_SEND_CONSUMER_GROUP);
        consumer.setNamesrvAddr(rocketMQProperties.getNameServer());
        consumer.subscribe(PLATFORM_PROMOTION_SEND_TOPIC, "*");
        consumer.registerMessageListener(platFormPromotionListener);
        return consumer;
    }
    /**
     * 热门商品推送
     * @param platFormHotProductListener
     * @return
     * @throws MQClientException
     */
    @Bean("platformHotProductSendTopicConsumer")
    public DefaultMQPushConsumer platformHotProductConsumer(PlatFormHotProductListener platFormHotProductListener) throws MQClientException {
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(PLATFORM_HOT_PRODUCT_SEND_CONSUMER_GROUP);
        consumer.setNamesrvAddr(rocketMQProperties.getNameServer());
        consumer.subscribe(PLATFORM_HOT_PRODUCT_SEND_TOPIC, "*");
        consumer.registerMessageListener(platFormHotProductListener);
        consumer.start();
        return consumer;
    }

    /**
     * 平台发放优惠券领取消费者
     * @param platformMessageListener
     * @return
     * @throws MQClientException
     */
    @Bean("platformMessageSendTopicConsumer")
    public DefaultMQPushConsumer receiveCouponConsumer(PlatformMessageListener platformMessageListener) throws MQClientException {
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(PLATFORM_MESSAGE_SEND_CONSUMER_GROUP);
        consumer.setNamesrvAddr(rocketMQProperties.getNameServer());
        consumer.subscribe(PLATFORM_MESSAGE_SEND_TOPIC, "*");
        consumer.registerMessageListener(platformMessageListener);
        consumer.start();
        return consumer;
    }

    @Bean("platFormConditionCouponConsumer")
    public DefaultMQPushConsumer platFormConditionCouponConsumer(
                    PlatFormConditionCouponListener platFormPromotionListener) throws MQClientException {
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(PLATFORM_CONDITION_COUPON_SEND_CONSUMER_GROUP);
        consumer.setNamesrvAddr(rocketMQProperties.getNameServer());
        consumer.subscribe(PLATFORM_CONDITION_COUPON_SEND_TOPIC, "*");
        consumer.registerMessageListener(platFormPromotionListener);
        consumer.start();
        return consumer;
    }

    @Bean("PlatFormHotProductUserBucketConsumer")
    public DefaultMQPushConsumer PlatFormHotProductUserBucketConsumer(
            PlatFormHotProductUserBucketListener platFormHotProductUserBucketListener) throws MQClientException {
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(PLATFORM_HOT_PRODUCT_USER_BUCKET_SEND_CONSUMER_GROUP);
        consumer.setNamesrvAddr(rocketMQProperties.getNameServer());
        consumer.subscribe(PLATFORM_HOT_PRODUCT_USER_BUCKET_SEND_TOPIC, "*");
        consumer.registerMessageListener(platFormHotProductUserBucketListener);
        consumer.start();
        return consumer;
    }
}
