package com.ruyuan.eshop.push.dao;

import com.ruyuan.eshop.common.dao.BaseDAO;
import com.ruyuan.eshop.push.domain.entity.HotGoodsCrontabDO;
import com.ruyuan.eshop.push.domain.mapper.HotGoodsCrontabMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zhonghuashishan
 */
@Slf4j
@Repository
public class HotGoodsCrontabDAO extends BaseDAO<HotGoodsCrontabMapper, HotGoodsCrontabDO> {

    @Resource
    private HotGoodsCrontabMapper hotGoodsCrontabMapper;

    public List<HotGoodsCrontabDO> queryHotGoodsCrontabByCrontabDate(Date crontabDate) {
        return hotGoodsCrontabMapper.queryHotGoodsCrontabByCrontabDate(crontabDate);
    }
}
