package com.ruyuan.eshop.push.domain.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 热门商品
 * @author zhonghuashishan
 */
@Data
public class HotGoodsVO implements Serializable{

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 商品关键词
     */
    private List<String> keyWords;
    /**
     * 商品名称
     */
    private String goodsDesc;

}
