package com.ruyuan.eshop.push.domain.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruyuan.eshop.push.domain.entity.MessagePushCrontabDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zhonghuashishan
 */
@Mapper
public interface MessagePushCrontabMapper extends BaseMapper<MessagePushCrontabDO> {
}
