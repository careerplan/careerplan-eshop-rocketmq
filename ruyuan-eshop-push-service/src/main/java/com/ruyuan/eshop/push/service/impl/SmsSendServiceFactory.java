package com.ruyuan.eshop.push.service.impl;

import com.ruyuan.eshop.common.message.PlatformMessagePushMessage;
import com.ruyuan.eshop.push.domain.dto.SmsMessageSendDTO;
import com.ruyuan.eshop.push.service.MessageSendService;
import com.ruyuan.eshop.push.service.MessageSendServiceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author zhonghuashishan
 */
@Component
public class SmsSendServiceFactory implements MessageSendServiceFactory {

    @Autowired
    private SmsSendServiceImpl smsSendServiceImpl;

    @Override
    public MessageSendService createMessageSendService() {
        return smsSendServiceImpl;
    }

    @Override
    public SmsMessageSendDTO createMessageSendDTO(PlatformMessagePushMessage platformMessagePushMessage) {
        SmsMessageSendDTO messageSendDTO = new SmsMessageSendDTO();
        messageSendDTO.setMainMessage(platformMessagePushMessage.getMainMessage());
        messageSendDTO.setMessage(platformMessagePushMessage.getMessage());
        messageSendDTO.setInformType(platformMessagePushMessage.getInformType());
        messageSendDTO.setUserAccountId(platformMessagePushMessage.getUserAccountId());
        return messageSendDTO;
    }
}
