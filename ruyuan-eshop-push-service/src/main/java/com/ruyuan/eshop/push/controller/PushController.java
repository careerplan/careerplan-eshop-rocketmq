package com.ruyuan.eshop.push.controller;

import com.alibaba.fastjson.JSON;
import com.ruyuan.eshop.common.core.JsonResult;
import com.ruyuan.eshop.common.exception.BaseBizException;
import com.ruyuan.eshop.common.utils.JsonUtil;
import com.ruyuan.eshop.push.domain.dto.QueryMessageDTO;
import com.ruyuan.eshop.push.domain.request.QueryMessageRequest;
import com.ruyuan.eshop.push.service.MessagePushService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author zhonghuashishan
 */
@Slf4j
@RestController
@RequestMapping(value = "/careerplan/push")
public class PushController {

    @Autowired
    private MessagePushService messagePushService;

    @RequestMapping
    public JsonResult<List<QueryMessageDTO>> queryMessageByCondition(@RequestBody QueryMessageRequest queryMessageRequest) {
        log.info("查询消息记录, QueryMessageRequest: {}", JsonUtil.object2Json(queryMessageRequest));

        List<QueryMessageDTO> dtos = messagePushService.queryMessageByCondition(queryMessageRequest);
        return JsonResult.buildSuccess(dtos);
    }

}
