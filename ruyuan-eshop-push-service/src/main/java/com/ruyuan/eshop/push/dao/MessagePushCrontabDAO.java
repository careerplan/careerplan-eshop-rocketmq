package com.ruyuan.eshop.push.dao;

import com.ruyuan.eshop.common.dao.BaseDAO;
import com.ruyuan.eshop.push.domain.entity.MessagePushCrontabDO;
import com.ruyuan.eshop.push.domain.mapper.MessagePushCrontabMapper;
import org.springframework.stereotype.Repository;

/**
 * @author zhonghuashishan
 */
@Repository
public class MessagePushCrontabDAO extends BaseDAO<MessagePushCrontabMapper, MessagePushCrontabDO> {

}
