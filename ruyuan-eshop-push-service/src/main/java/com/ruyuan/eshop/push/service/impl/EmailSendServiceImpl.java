package com.ruyuan.eshop.push.service.impl;

import com.ruyuan.eshop.push.domain.dto.EmailMessageSendDTO;
import com.ruyuan.eshop.push.service.MessageSendService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author zhonghuashishan
 */
@Slf4j
@Component
public class EmailSendServiceImpl implements MessageSendService<EmailMessageSendDTO> {


    @Override
    public Boolean send(EmailMessageSendDTO messagePushDTO) {
        log.info("邮件发送中:{}", messagePushDTO);
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            log.error("邮件发送失败:{}",e);
        }
        return true;
    }
}
