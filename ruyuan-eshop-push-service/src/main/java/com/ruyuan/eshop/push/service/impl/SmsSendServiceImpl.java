package com.ruyuan.eshop.push.service.impl;

import com.ruyuan.eshop.push.domain.dto.SmsMessageSendDTO;
import com.ruyuan.eshop.push.service.MessageSendService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author zhonghuashishan
 */
@Slf4j
@Component
public class SmsSendServiceImpl implements MessageSendService<SmsMessageSendDTO> {


    @Override
    public Boolean send(SmsMessageSendDTO messagePushDTO) {
        log.info("短信发送中:{}", messagePushDTO);
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            log.error("短信发送中失败:{}",e);
        }
        return true;
    }
}
