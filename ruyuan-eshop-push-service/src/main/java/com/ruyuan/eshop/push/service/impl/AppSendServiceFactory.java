package com.ruyuan.eshop.push.service.impl;

import com.ruyuan.eshop.common.message.PlatformMessagePushMessage;
import com.ruyuan.eshop.push.domain.dto.AppMessageSendDTO;
import com.ruyuan.eshop.push.domain.dto.MessageSendDTO;
import com.ruyuan.eshop.push.service.MessageSendService;
import com.ruyuan.eshop.push.service.MessageSendServiceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author zhonghuashishan
 */
@Component
public class AppSendServiceFactory implements MessageSendServiceFactory {

    @Autowired
    private AppSendServiceImpl appSendServiceImpl;

    @Override
    public MessageSendService createMessageSendService() {
        return appSendServiceImpl;
    }

    @Override
    public MessageSendDTO createMessageSendDTO(PlatformMessagePushMessage platformMessagePushMessage) {
        AppMessageSendDTO messageSendDTO = new AppMessageSendDTO();
        messageSendDTO.setMainMessage(platformMessagePushMessage.getMainMessage());
        messageSendDTO.setMessage(platformMessagePushMessage.getMessage());
        messageSendDTO.setInformType(platformMessagePushMessage.getInformType());
        messageSendDTO.setUserAccountId(platformMessagePushMessage.getUserAccountId());
        return messageSendDTO;
    }
}
