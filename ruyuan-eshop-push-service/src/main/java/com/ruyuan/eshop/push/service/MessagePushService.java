package com.ruyuan.eshop.push.service;

import com.ruyuan.eshop.membership.domain.dto.MembershipFilterDTO;
import com.ruyuan.eshop.push.domain.dto.PushMessageDTO;
import com.ruyuan.eshop.push.domain.dto.QueryMessageDTO;
import com.ruyuan.eshop.push.domain.dto.SaveOrUpdateMessageDTO;
import com.ruyuan.eshop.push.domain.dto.SendMessageDTO;
import com.ruyuan.eshop.push.domain.request.QueryMessageRequest;
import com.ruyuan.eshop.push.domain.request.SaveOrUpdateMessageRequest;
import com.ruyuan.eshop.push.domain.request.SendMessageByAccountRequest;

import java.util.List;

/**
 * @author zhonghuashishan
 */
public interface MessagePushService {

    /**
     * 新增/修改消息推送
     *
     * @param saveOrUpdateMessageRequest
     * @return
     */
    SaveOrUpdateMessageDTO saveOrUpdateMessage(SaveOrUpdateMessageRequest saveOrUpdateMessageRequest);

    /**
     * 查询消息记录
     *
     * @param queryMessageRequest
     * @return
     * @author zhonghuashishan
     */
    List<QueryMessageDTO> queryMessageByCondition(QueryMessageRequest queryMessageRequest);

    /**
     * 根据accountId单点推送消息
     *
     * @param sendMessageByAccountRequest
     * @return
     */
    SendMessageDTO sendMessageByAccount(SendMessageByAccountRequest sendMessageByAccountRequest);

    /**
     * 推送消息
     *
     * @param pushMessageDTO
     * @param membershipFilterDTO
     */
    void pushMessages(PushMessageDTO pushMessageDTO, MembershipFilterDTO membershipFilterDTO);
}
