package com.ruyuan.eshop.push.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @author zhonghuashishan
 */
@Data
@TableName("push_message_crontab")
public class MessagePushCrontabDO {

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 消息id
     */
    private Long messageId;

    /**
     * 消息主题
     */
    private String mainMessage;

    /**
     * 推送类型：1定时推送，2直接推送
     */
    private Integer pushType;

    /**
     * 1：短信，2：app消息，3：邮箱
     */
    private Integer informType;

    /**
     * 推送消息内容
     */
    private String messageInfo;

    /**
     * 选人条件
     */
    private String filterCondition;

    /**
     * 定时任务时间
     */
    private Date crontabTime;

    /**
     * 发送周期序号
     */
    private Integer periodSendNumber;

    /**
     * 是否已执行
     */
    private Integer executeFlag;

    private Integer createUser;

    private Date createTime;

    private Integer updateUser;

    private Date updateTime;
}
