package com.ruyuan.eshop.push.api.impl;

import com.alibaba.fastjson.JSON;
import com.ruyuan.eshop.common.core.JsonResult;
import com.ruyuan.eshop.common.exception.BaseBizException;
import com.ruyuan.eshop.push.api.MessagePushApi;
import com.ruyuan.eshop.push.domain.dto.QueryMessageDTO;
import com.ruyuan.eshop.push.domain.dto.SaveOrUpdateMessageDTO;
import com.ruyuan.eshop.push.domain.dto.SendMessageDTO;
import com.ruyuan.eshop.push.domain.request.QueryMessageRequest;
import com.ruyuan.eshop.push.domain.request.SaveOrUpdateMessageRequest;
import com.ruyuan.eshop.push.domain.request.SendMessageByAccountRequest;
import com.ruyuan.eshop.push.service.MessagePushService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.List;

/**
 * @author zhonghuashishan
 */
@Slf4j
@DubboService(version = "1.0.0", interfaceClass = MessagePushApi.class, retries = 0)
public class MessagePushApiImpl implements MessagePushApi {

    @Autowired
    private MessagePushService messagePushService;

    @Override
    public JsonResult<SaveOrUpdateMessageDTO> saveOrUpdateMessage(SaveOrUpdateMessageRequest saveOrUpdateMessageRequest) {
        try {
            SaveOrUpdateMessageDTO dto = messagePushService.saveOrUpdateMessage(saveOrUpdateMessageRequest);
            return JsonResult.buildSuccess(dto);
        } catch (BaseBizException e) {
            log.error("biz error: request={}", JSON.toJSONString(saveOrUpdateMessageRequest), e);
            return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            log.error("system error: request={}", JSON.toJSONString(saveOrUpdateMessageRequest), e);
            return JsonResult.buildError(e.getMessage());
        }
    }

    @Override
    public JsonResult<SendMessageDTO> sendMessageByAccount(SendMessageByAccountRequest sendMessageByAccountRequest) {
        try {
            SendMessageDTO dto = messagePushService.sendMessageByAccount(sendMessageByAccountRequest);
            return JsonResult.buildSuccess(dto);
        } catch (BaseBizException e) {
            log.error("biz error: request={}", JSON.toJSONString(sendMessageByAccountRequest), e);
            return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            log.error("system error: request={}", JSON.toJSONString(sendMessageByAccountRequest), e);
            return JsonResult.buildError(e.getMessage());
        }
    }

    @Override
    public JsonResult<List<QueryMessageDTO>> queryMessageByCondition(QueryMessageRequest queryMessageRequest) {
        try {
            List<QueryMessageDTO> dtos = messagePushService.queryMessageByCondition(queryMessageRequest);
            return JsonResult.buildSuccess(dtos);
        } catch (BaseBizException e) {
            log.error("biz error: request={}", JSON.toJSONString(queryMessageRequest), e);
            return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            log.error("system error: request={}", JSON.toJSONString(queryMessageRequest), e);
            return JsonResult.buildError(e.getMessage());
        }
    }

}
