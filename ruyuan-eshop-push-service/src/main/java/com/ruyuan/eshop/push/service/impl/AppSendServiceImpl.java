package com.ruyuan.eshop.push.service.impl;

import com.ruyuan.eshop.push.domain.dto.AppMessageSendDTO;
import com.ruyuan.eshop.push.service.MessageSendService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author zhonghuashishan
 */
@Slf4j
@Component
public class AppSendServiceImpl implements MessageSendService<AppMessageSendDTO> {

    @Override
    public Boolean send(AppMessageSendDTO messagePushDTO) {
        log.info("app消息推送中:{}", messagePushDTO);
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            log.error("app消息推送失败:{}",e);
        }
        return true;
    }
}
