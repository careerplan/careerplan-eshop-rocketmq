package com.ruyuan.eshop.push.service.impl;

import com.ruyuan.eshop.common.exception.BaseBizException;
import com.ruyuan.eshop.push.enums.InformTypeEnum;
import com.ruyuan.eshop.push.service.MessageSendServiceFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;


/**
 * @author zhonghuashishan
 */
@Component
@Slf4j
public class FactoryProducer {
    /**
     * 短信发送服务工厂
     */
    @Autowired
    private SmsSendServiceFactory smsSendServiceFactory;

    /**
     * app消息通知发送服务工厂
     */
    @Autowired
    private AppSendServiceFactory appSendServiceFactory;

    /**
     * email消息通知发送服务工厂
     */
    @Autowired
    private EmailSendServiceFactory emailSendServiceFactory;

    /**
     * 根据消息类型，获取消息发送服务工厂
     *
     * @param informType 消息类型
     * @return 消息发送服务工厂
     */
    public MessageSendServiceFactory getMessageSendServiceFactory(Integer informType){

        if (Objects.isNull(informType)){
            log.info("通知类型为null，默认走APP消息推送");
            return appSendServiceFactory;
        }
        if (InformTypeEnum.SMS.getCode().equals(informType)) {
            return smsSendServiceFactory;
        }else if (InformTypeEnum.APP.getCode().equals(informType)) {
            return appSendServiceFactory;
        }else if (InformTypeEnum.EMAIL.getCode().equals(informType)) {
            return emailSendServiceFactory;
        }
        throw new BaseBizException("参数异常");
    }
}
