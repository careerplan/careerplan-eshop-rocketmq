package com.ruyuan.eshop.push.service.impl;

import com.ruyuan.eshop.common.message.PlatformMessagePushMessage;
import com.ruyuan.eshop.push.domain.dto.EmailMessageSendDTO;
import com.ruyuan.eshop.push.service.MessageSendService;
import com.ruyuan.eshop.push.service.MessageSendServiceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author zhonghuashishan
 */
@Component
public class EmailSendServiceFactory implements MessageSendServiceFactory {

    @Autowired
    private EmailSendServiceImpl emailSendServiceImpl;

    @Override
    public MessageSendService createMessageSendService() {
        return emailSendServiceImpl;
    }

    @Override
    public EmailMessageSendDTO createMessageSendDTO(PlatformMessagePushMessage platformMessagePushMessage) {
        EmailMessageSendDTO messageSendDTO = new EmailMessageSendDTO();
        messageSendDTO.setMainMessage(platformMessagePushMessage.getMainMessage());
        messageSendDTO.setMessage(platformMessagePushMessage.getMessage());
        messageSendDTO.setInformType(platformMessagePushMessage.getInformType());
        messageSendDTO.setUserAccountId(platformMessagePushMessage.getUserAccountId());
        return messageSendDTO;
    }
}
