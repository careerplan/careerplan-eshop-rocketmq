package com.ruyuan.eshop.push.service;

import com.ruyuan.eshop.common.message.PlatformMessagePushMessage;
import com.ruyuan.eshop.push.domain.dto.MessageSendDTO;

/**
 * 消息推送服务工厂
 *
 * @author zhonghuashishan
 */
public interface MessageSendServiceFactory {

    /**
     * 创建消息推送服务组件
     *
     * @return
     */
    MessageSendService createMessageSendService();

    /**
     * 创建消息推送DTO
     * 不同消息类型，可以构建不同的消息推送DTO
     *
     * @param platformMessagePushMessage
     * @return
     */
    MessageSendDTO createMessageSendDTO(PlatformMessagePushMessage platformMessagePushMessage);
}
