package com.ruyuan.eshop.push.converter;

import com.ruyuan.eshop.push.domain.dto.QueryMessageDTO;
import com.ruyuan.eshop.push.domain.entity.MessagePushCrontabDO;
import com.ruyuan.eshop.push.domain.entity.MessagePushDO;
import com.ruyuan.eshop.push.domain.request.QueryMessageRequest;
import com.ruyuan.eshop.push.domain.request.SaveOrUpdateMessageRequest;
import com.ruyuan.eshop.push.domain.request.SendMessageByAccountRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

/**
 * @author zhonghuashishan
 */
@Mapper(componentModel = "spring")
public interface MessagePushConverter {

    /**
     * 转换对象
     * @param request 对象
     * @return 对象
     */
    @Mapping(target = "messageInfo", source = "message")
    MessagePushDO requestToEntity(QueryMessageRequest request);

    /**
     * 转换对象
     * @param dto 对象
     * @return 对象
     */
    @Mapping(target = "message", source = "messageInfo")
    QueryMessageDTO entityToDTO(MessagePushDO dto);

    /**
     * 转换对象
     * @param list 对象
     * @return 对象
     */
    List<QueryMessageDTO> listEntityToDTO(List<MessagePushDO> list);

    /**
     * 转换对象
     * @param request 对象
     * @return 对象
     */
    @Mapping(target = "messageInfo", source = "message")
    MessagePushDO convertMessageDO(SaveOrUpdateMessageRequest request);

    /**
     * 转换对象
     * @param request 对象
     * @return 对象
     */
    @Mapping(target = "messageInfo", source = "message")
    MessagePushDO convertMessageDO(SendMessageByAccountRequest request);


    /**
     * 转换对象
     * @param request 对象
     * @return 对象
     */
    @Mapping(target = "messageInfo", source = "message")
    MessagePushCrontabDO convertMessageCrontabDO(SaveOrUpdateMessageRequest request);
}
