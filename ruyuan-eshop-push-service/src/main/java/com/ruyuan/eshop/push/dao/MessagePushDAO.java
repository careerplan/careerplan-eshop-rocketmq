package com.ruyuan.eshop.push.dao;

import com.ruyuan.eshop.common.dao.BaseDAO;
import com.ruyuan.eshop.push.domain.entity.MessagePushDO;
import com.ruyuan.eshop.push.domain.mapper.MessagePushMapper;
import org.springframework.stereotype.Repository;

/**
 * @author zhonghuashishan
 */
@Repository
public class MessagePushDAO extends BaseDAO<MessagePushMapper, MessagePushDO> {
}
