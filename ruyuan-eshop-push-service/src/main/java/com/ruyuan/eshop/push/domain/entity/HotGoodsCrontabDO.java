package com.ruyuan.eshop.push.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @author zhonghuashishan
 */
@Data
@TableName("hot_goods_crontab")
public class HotGoodsCrontabDO {
    /**
     * 主键
     */
    private Long id;

    /**
     * 热门商品id
     */
    private Long goodsId;

    /**
     * 热门商品名
     */
    private String goodsName;

    /**
     * 热门商品描述
     */
    private String goodsDesc;

    /**
     * 关键字
     */
    private String keywords;

    /**
     * 定时任务日期
     */
    private Date crontabDate;

    /**
     * 用户画像，json格式，简化业务处理，和热门商品处于一对一关系
     */
    private String portrayal;

    /**
     * 定时任务执行分片后缀 1-9，根据
     */
    private Integer jobShardSuffix;

    private Integer createUser;

    private Date createTime;

    private Integer updateUser;

    private Date updateTime;
}
