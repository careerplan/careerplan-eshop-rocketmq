package com.ruyuan.eshop.push.service;

import com.ruyuan.eshop.push.domain.dto.MessageSendDTO;

/**
 * @author zhonghuashishan
 */
public interface MessageSendService<T extends MessageSendDTO>  {

    /**
     * 消息发送
     * @return
     * @param messagePushDTO
     */
    Boolean send(T messagePushDTO);
}
