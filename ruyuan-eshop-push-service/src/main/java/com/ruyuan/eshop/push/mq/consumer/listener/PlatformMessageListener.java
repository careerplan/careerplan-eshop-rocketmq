package com.ruyuan.eshop.push.mq.consumer.listener;

import com.alibaba.fastjson.JSON;;
import com.ruyuan.eshop.common.message.PlatformMessagePushMessage;
import com.ruyuan.eshop.promotion.domain.dto.SalesPromotionCouponItemDTO;
import com.ruyuan.eshop.push.domain.dto.MessageSendDTO;
import com.ruyuan.eshop.push.service.MessageSendService;
import com.ruyuan.eshop.push.service.MessageSendServiceFactory;
import com.ruyuan.eshop.push.service.impl.FactoryProducer;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.common.message.MessageExt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhonghuashishan
 */
@Slf4j
@Component
public class PlatformMessageListener implements MessageListenerConcurrently {

    /**
     * 消息推送工厂提供者
     */
    @Autowired
    private FactoryProducer factoryProducer;


    /**
     * 并发消费消息
     * @param msgList
     * @param context
     * @return
     */
    @Override
    public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgList, ConsumeConcurrentlyContext context) {
        try {
            List<SalesPromotionCouponItemDTO> list = new ArrayList<>();
            for (MessageExt messageExt : msgList) {
                log.debug("执行平台消息推送消费消息逻辑，消息内容：{}", messageExt.getBody());
                String msg = new String(messageExt.getBody());
                PlatformMessagePushMessage platformMessagePushMessage = JSON.parseObject(msg , PlatformMessagePushMessage.class);
                log.info("开始消息推送，platformMessagePushMessage:{}", platformMessagePushMessage);

                // 获取消息服务工厂
                MessageSendServiceFactory messageSendServiceFactory = factoryProducer
                        .getMessageSendServiceFactory(platformMessagePushMessage.getInformType());

                // 消息发送服务组件
                MessageSendService messageSendService = messageSendServiceFactory
                        .createMessageSendService();

                // 创建消息发送DTO
                MessageSendDTO messageSendDTO = messageSendServiceFactory
                        .createMessageSendDTO(platformMessagePushMessage);

                // 发送消息
                messageSendService.send(messageSendDTO);
                log.info("消息推送完成，messageSendDTO:{}", messageSendDTO);
            }
        }catch (Exception e){
            log.error("consume error,平台消息推送消费失败", e);
            // 本次消费失败，下次重新消费
            return ConsumeConcurrentlyStatus.RECONSUME_LATER;
        }

        return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
    }

}
