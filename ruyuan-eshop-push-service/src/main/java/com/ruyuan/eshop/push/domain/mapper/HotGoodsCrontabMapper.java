package com.ruyuan.eshop.push.domain.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruyuan.eshop.push.domain.entity.HotGoodsCrontabDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.List;

/**
 * @author zhonghuashishan
 */
@Mapper
public interface HotGoodsCrontabMapper extends BaseMapper<HotGoodsCrontabDO> {

    /**
     * 根据日期找到今日热门商品推送任务
     * @param crontabDate
     * @return
     */
    @Select("select * from hot_goods_crontab where TO_DAYS(crontab_date) = TO_DAYS(#{crontabDate})")
    List<HotGoodsCrontabDO> queryHotGoodsCrontabByCrontabDate(@Param("crontabDate") Date crontabDate);
}
