package com.ruyuan.eshop.push.domain.dto;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

/**
 * @author zhonghuashishan
 */
@Data
@Builder
public class PushMessageDTO {

    /**
     * 消息类型
     */
    private String mainMessage;

    /**
     * 消息内容
     */
    private String message;

    /**
     * 消息类型
     */
    private Integer informType;

    @Tolerate
    public PushMessageDTO() {

    }

}
