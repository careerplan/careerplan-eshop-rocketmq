package com.ruyuan.eshop.push.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @author zhonghuashishan
 */
@Data
@TableName("push_message")
public class MessagePushDO {

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 消息主题
     */
    private String mainMessage;

    /**
     * 推送类型：1定时推送，2直接推送
     */
    private Integer pushType;

    /**
     * 1：短信，2：app消息，3：邮箱
     */
    private Integer informType;

    /**
     * 推送消息内容
     */
    private String messageInfo;

    /**
     * 选人条件
     */
    private String filterCondition;

    /**
     * 定时发送消息任务开始时间
     */
    private Date pushStartTime;

    /**
     * 定时发送任务结束时间
     */
    private Date pushEndTime;

    /**
     * 每个发送周期内的发送次数，以此为依据发送消息
     */
    private Integer sendPeriodCount;

    private Integer createUser;

    private Date createTime;

    private Integer updateUser;

    private Date updateTime;
}
