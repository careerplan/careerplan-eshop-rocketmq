package com.ruyuan.eshop.push.domain.dto;

import lombok.Data;

/**
 * @author zhonghuashishan
 */
@Data
public class EmailMessageSendDTO extends MessageSendDTO {

    /**
     * 邮箱
     */
    private String email;

}
