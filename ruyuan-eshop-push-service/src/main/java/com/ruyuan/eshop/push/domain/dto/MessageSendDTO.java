package com.ruyuan.eshop.push.domain.dto;

import lombok.Data;

/**
 * @author zhonghuashishan
 */
@Data
public class MessageSendDTO {

    /**
     * 主题
     */
    private String mainMessage;

    /**
     * 消息内容
     */
    private String message;

    /**
     * 消息类型
     */
    private Integer informType;

    /**
     * 用户id
     */
    private Long userAccountId;

}
