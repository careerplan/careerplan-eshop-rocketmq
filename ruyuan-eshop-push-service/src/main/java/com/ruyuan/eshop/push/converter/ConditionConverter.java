package com.ruyuan.eshop.push.converter;

import com.ruyuan.eshop.membership.domain.dto.MembershipFilterConditionDTO;
import com.ruyuan.eshop.membership.domain.dto.MembershipFilterDTO;
import com.ruyuan.eshop.persona.domain.dto.PersonaFilterConditionDTO;
import org.mapstruct.Mapper;

/**
 * @author zhonghuashishan
 */
@Mapper(componentModel = "spring")
public interface ConditionConverter {
    /**
     * 用户画像查询条件转换器
     * @param conditionDTO
     * @return
     */
    PersonaFilterConditionDTO convertFilterCondition(MembershipFilterConditionDTO conditionDTO);

    /**
     * 用户画像查询条件转换器
     * @param conditionDTO
     * @return
     */
    PersonaFilterConditionDTO convertFilterCondition(MembershipFilterDTO conditionDTO);
}
