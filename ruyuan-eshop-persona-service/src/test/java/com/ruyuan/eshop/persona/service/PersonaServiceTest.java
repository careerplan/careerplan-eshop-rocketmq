package com.ruyuan.eshop.persona.service;

import com.ruyuan.eshop.persona.PersonaApplication;
import com.ruyuan.eshop.persona.dao.MembershipFilterDAO;
import com.ruyuan.eshop.persona.domain.dto.PersonaFilterConditionDTO;
import com.ruyuan.eshop.persona.page.PersonaConditionPage;
import com.ruyuan.eshop.persona.page.PersonaConditionWithIdRange;
import junit.framework.TestCase;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * @author zhonghuashishan
 */
@Slf4j
@SpringBootTest(classes = PersonaApplication.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class PersonaServiceTest extends TestCase {

    @Autowired
    private MembershipFilterDAO membershipFilterDAO;

    @Test
    public void testQueryMaxIdByCondition() {
        PersonaFilterConditionDTO conditionDTO = PersonaFilterConditionDTO.builder()
                .accountType(1)
                .memberLevel(1)
                //.activeCount(3)
                //.memberPoint(1200L)
                //.totalActiveCount(5)
                //.totalAmount(76662)
                .build();
        Long maxId = membershipFilterDAO.getBaseMapper().queryMaxUserId(conditionDTO);
        Assert.assertNotNull(maxId);
        Assert.assertEquals(4L,maxId.longValue());
    }

    @Test
    public void testQueryMinIdByCondition() {
        PersonaFilterConditionDTO conditionDTO = PersonaFilterConditionDTO.builder()
                .accountType(1)
                .memberLevel(1)
                //.activeCount(3)
                .memberPoint(1200L)
                //.totalActiveCount(5)
                //.totalAmount(76662)
                .build();
        Long maxId = membershipFilterDAO.getBaseMapper().queryMinUserId(conditionDTO);
        Assert.assertNotNull(maxId);
        Assert.assertEquals(1L,maxId.longValue());
    }

    @Test
    public void testGetAccountIdsByIdRange() {
        PersonaConditionWithIdRange range = PersonaConditionWithIdRange.builder()
                .endId(1551207668L)
                .startId(1L)
                .memberLevel(1)
                .memberPoint(123L)
                .build();
        List<Long> list = membershipFilterDAO.getBaseMapper().getAccountIdsByIdRange(range);
        log.info("账号id信息：{}",list);
        Assert.assertNotNull(list);
    }

    @Test
    public void testCountByCondition() {
        PersonaFilterConditionDTO conditionDTO = PersonaFilterConditionDTO.builder()
                .accountType(1)
                .memberLevel(1)
                .activeCount(3)
                .totalActiveCount(5)
                .totalAmount(76662)
                .build();
        Integer count = membershipFilterDAO.getBaseMapper().countUserIds(conditionDTO);
        Assert.assertNotNull(count);
        Assert.assertEquals(3,count.intValue());
    }

    @Test
    public void testGetAccountIdsByIdLimit() {
        PersonaConditionPage page = PersonaConditionPage.builder()
                .accountType(1)
                .memberLevel(1)
                .activeCount(3)
                .totalActiveCount(5)
                .totalAmount(76662)
                .offset(0L)
                .limit(10)
                .build();
        List<Long> accountIds = membershipFilterDAO.getBaseMapper().listAccountIdByPage(page);
        log.info("分页查询到的id:{}",accountIds);
        Assert.assertNotNull(accountIds);
    }
}