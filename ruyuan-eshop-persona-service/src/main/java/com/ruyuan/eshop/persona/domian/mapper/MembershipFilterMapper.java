package com.ruyuan.eshop.persona.domian.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruyuan.eshop.persona.domain.dto.PersonaFilterConditionDTO;
import com.ruyuan.eshop.persona.domian.entity.MembershipFilterDO;
import com.ruyuan.eshop.persona.page.PersonaConditionPage;
import com.ruyuan.eshop.persona.page.PersonaConditionWithIdRange;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author zhonghuashishan
 */
@Mapper
public interface MembershipFilterMapper extends BaseMapper<MembershipFilterDO> {

    /**
     * 根据查询条件查询最大id
     * @param conditionDTO
     * @return
     */
    @Select("<script>" +
            "SELECT MAX(account_id) from membership_filter " +
            "WHERE account_type = 1" +
            "<if test='memberLevel != null'>" +
            " AND membership_level = #{memberLevel} " +
            "</if>" +
            "<if test='activeCount != null'>" +
            " AND active_count = #{activeCount} " +
            "</if>" +
            "<if test='totalActiveCount != null'>" +
            " AND total_active_count = #{totalActiveCount} " +
            "</if>" +
            "<if test='totalAmount != null'>" +
            " AND total_amount = #{totalAmount} " +
            "</if>"
            +"</script>"
    )
    Long queryMaxUserId(PersonaFilterConditionDTO conditionDTO);

    /**
     * 根据查询条件查询最小id
     * @param conditionDTO
     * @return
     */
    @Select("<script>" +
            "SELECT MIN(account_id) from membership_filter " +
            "WHERE account_type = 1" +
            "<if test='memberLevel != null'>" +
            " AND membership_level = #{memberLevel} " +
            "</if>" +
            "<if test='activeCount != null'>" +
            " AND active_count = #{activeCount} " +
            "</if>" +
            "<if test='totalActiveCount != null'>" +
            " AND total_active_count = #{totalActiveCount} " +
            "</if>" +
            "<if test='totalAmount != null'>" +
            " AND total_amount = #{totalAmount} " +
            "</if>"
            +"</script>"
    )
    Long queryMinUserId(PersonaFilterConditionDTO conditionDTO);

    /**
     * 根据查询条件查询用户数量
     * @param conditionDTO
     * @return
     */
    @Select("<script>" +
            "SELECT COUNT(account_id) from membership_filter " +
            "WHERE account_type = 1" +
            "<if test='memberLevel != null'>" +
            " AND membership_level = #{memberLevel} " +
            "</if>" +
            "<if test='activeCount != null'>" +
            " AND active_count = #{activeCount} " +
            "</if>" +
            "<if test='totalActiveCount != null'>" +
            " AND total_active_count = #{totalActiveCount} " +
            "</if>" +
            "<if test='totalAmount != null'>" +
            " AND total_amount = #{totalAmount} " +
            "</if>" +
            "</script>"
    )
    Integer countUserIds(PersonaFilterConditionDTO conditionDTO);

    /**
     * 分页查询用户id
     * @param page
     * @return
     */
    @Select("<script>" +
            "SELECT account_id from membership_filter " +
            "WHERE account_type = 1" +
            "<if test='memberLevel != null'>" +
            " AND membership_level = #{memberLevel} " +
            "</if>" +
            "<if test='activeCount != null'>" +
            " AND active_count = #{activeCount} " +
            "</if>" +
            "<if test='totalActiveCount != null'>" +
            " AND total_active_count = #{totalActiveCount} " +
            "</if>" +
            "<if test='totalAmount != null'>" +
            " AND total_amount = #{totalAmount} " +
            "</if>" +
            "ORDER BY account_id " +
            "LIMIT #{offset},#{limit}" +
            "</script>"
    )
    List<Long> listAccountIdByPage(PersonaConditionPage page);


    /**
     * 根据id范围查询用户id
     * @param range
     * @return
     */
    @Select("<script>" +
            "SELECT account_id from membership_point " +
            "WHERE 1 = 1" +
            "<if test='startId != null'>" +
            " AND account_id &gt;= #{startId} " +
            "</if>" +
            "<if test='endId != null'>" +
            " AND account_id &lt;= #{endId} " +
            "</if>" +
            "<if test='memberLevel != null'>" +
            " AND member_level = #{memberLevel} " +
            "</if>" +
            "<if test='memberPoint != null'>" +
            " AND member_point = #{memberPoint} " +
            "</if>" +
            "</script>"
    )
    List<Long> getAccountIdsByIdRange(PersonaConditionWithIdRange range);

}
