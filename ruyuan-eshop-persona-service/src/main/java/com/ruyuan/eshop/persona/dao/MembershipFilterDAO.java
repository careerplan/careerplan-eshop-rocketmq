package com.ruyuan.eshop.persona.dao;

import com.ruyuan.eshop.common.dao.BaseDAO;
import com.ruyuan.eshop.persona.domian.entity.MembershipFilterDO;
import com.ruyuan.eshop.persona.domian.mapper.MembershipFilterMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

/**
 * @author zhonghuashishan
 */
@Slf4j
@Repository
public class MembershipFilterDAO extends BaseDAO<MembershipFilterMapper, MembershipFilterDO> {
}
