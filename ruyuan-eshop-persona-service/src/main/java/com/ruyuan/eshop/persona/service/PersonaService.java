package com.ruyuan.eshop.persona.service;

import com.ruyuan.eshop.common.core.JsonResult;
import com.ruyuan.eshop.persona.domain.dto.PersonaFilterConditionDTO;
import com.ruyuan.eshop.persona.page.PersonaConditionPage;
import com.ruyuan.eshop.persona.page.PersonaConditionWithIdRange;

import java.util.List;

/**
 * @author zhonghuashishan
 */
public interface PersonaService {
    /**
     * 查询最大id
     * @param personaFilterCondition
     * @return
     */
    Long queryMaxIdByCondition(PersonaFilterConditionDTO personaFilterCondition);

    /**
     * 查询最小id
     * @param personaFilterCondition
     * @return
     */
    Long queryMinIdByCondition(PersonaFilterConditionDTO personaFilterCondition);

    /**
     * 根据id范围查询用户id
     * @param personaConditionWithIdRange
     * @return
     */
    List<Long> getAccountIdsByIdRange(PersonaConditionWithIdRange personaConditionWithIdRange);

    /**
     * 查询用户数量
     * @param personaFilterCondition
     * @return
     */
    Integer countByCondition(PersonaFilterConditionDTO personaFilterCondition);

    /**
     * 分页查询用户id
     * @param page
     * @return
     */
    List<Long> getAccountIdsByIdLimit(PersonaConditionPage page);
}
