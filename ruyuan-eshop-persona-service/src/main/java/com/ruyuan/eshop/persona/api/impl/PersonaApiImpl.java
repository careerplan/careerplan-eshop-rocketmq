package com.ruyuan.eshop.persona.api.impl;

import com.alibaba.fastjson.JSON;
import com.ruyuan.eshop.common.core.JsonResult;
import com.ruyuan.eshop.common.exception.BaseBizException;
import com.ruyuan.eshop.persona.api.PersonaApi;
import com.ruyuan.eshop.persona.domain.dto.PersonaFilterConditionDTO;
import com.ruyuan.eshop.persona.page.PersonaConditionPage;
import com.ruyuan.eshop.persona.page.PersonaConditionWithIdRange;
import com.ruyuan.eshop.persona.service.PersonaService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author zhonghuashishan
 */
@Slf4j
@DubboService(version = "1.0.0", interfaceClass = PersonaApi.class, retries = 0)
public class PersonaApiImpl implements PersonaApi{

    @Autowired
    private PersonaService personaService;
    /**
     * 获取最大用户Id
     *
     * @param personaFilterCondition
     * @return
     */
    @Override
    public JsonResult<Long> queryMaxIdByCondition(PersonaFilterConditionDTO personaFilterCondition) {
        try {
            Long maxId = personaService.queryMaxIdByCondition(personaFilterCondition);
            return JsonResult.buildSuccess(maxId);
        } catch (BaseBizException e) {
            log.error("biz error,查询maxId出错: condition={}", JSON.toJSONString(personaFilterCondition), e);
            return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            log.error("system error,查询maxId出错: condition={}", JSON.toJSONString(personaFilterCondition), e);
            return JsonResult.buildError(e.getMessage());
        }
    }

    /**
     * 获取最小用户Id
     *
     * @param personaFilterCondition
     * @return
     */
    @Override
    public JsonResult<Long> queryMinIdByCondition(PersonaFilterConditionDTO personaFilterCondition) {
        try {
            Long maxId = personaService.queryMinIdByCondition(personaFilterCondition);
            return JsonResult.buildSuccess(maxId);
        } catch (BaseBizException e) {
            log.error("biz error,查询minId出错: condition={}", JSON.toJSONString(personaFilterCondition), e);
            return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            log.error("system error,查询minId出错: condition={}", JSON.toJSONString(personaFilterCondition), e);
            return JsonResult.buildError(e.getMessage());
        }
    }

    /**
     * 根据id范围查询用户账号id
     *
     * @param personaConditionWithIdRange
     * @return
     */
    @Override
    public JsonResult<List<Long>> getAccountIdsByIdRange(PersonaConditionWithIdRange personaConditionWithIdRange) {
        try {
            List<Long> accountIds = personaService.getAccountIdsByIdRange(personaConditionWithIdRange);
            return JsonResult.buildSuccess(accountIds);
        } catch (BaseBizException e) {
            log.error("biz error,根据id范围查询用户账号id出错: condition={}", JSON.toJSONString(personaConditionWithIdRange), e);
            return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            log.error("system error,根据id范围查询用户账号id出错: condition={}", JSON.toJSONString(personaConditionWithIdRange), e);
            return JsonResult.buildError(e.getMessage());
        }
    }

    /**
     * 根据选人条件获取count值
     *
     * @param personaFilterCondition
     * @return
     */
    @Override
    public JsonResult<Integer> countByCondition(PersonaFilterConditionDTO personaFilterCondition) {
        try {
            Integer count = personaService.countByCondition(personaFilterCondition);
            return JsonResult.buildSuccess(count);
        } catch (BaseBizException e) {
            log.error("biz error,查询count出错: condition={}", JSON.toJSONString(personaFilterCondition), e);
            return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            log.error("system error,查询count出错: condition={}", JSON.toJSONString(personaFilterCondition), e);
            return JsonResult.buildError(e.getMessage());
        }
    }

    /**
     * 分页获取用户数据
     *
     * @param page
     * @return
     */
    @Override
    public JsonResult<List<Long>> getAccountIdsByIdLimit(PersonaConditionPage page) {
        try {
            List<Long> accountIds = personaService.getAccountIdsByIdLimit(page);
            return JsonResult.buildSuccess(accountIds);
        } catch (BaseBizException e) {
            log.error("biz error,分页查询用户账号id出错: condition={}", JSON.toJSONString(page), e);
            return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            log.error("system error,分页查询查询用户账号id出错: condition={}", JSON.toJSONString(page), e);
            return JsonResult.buildError(e.getMessage());
        }
    }
}
