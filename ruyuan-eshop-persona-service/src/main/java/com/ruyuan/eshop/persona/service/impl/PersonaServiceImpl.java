package com.ruyuan.eshop.persona.service.impl;

import com.ruyuan.eshop.persona.dao.MembershipFilterDAO;
import com.ruyuan.eshop.persona.domain.dto.PersonaFilterConditionDTO;
import com.ruyuan.eshop.persona.page.PersonaConditionPage;
import com.ruyuan.eshop.persona.page.PersonaConditionWithIdRange;
import com.ruyuan.eshop.persona.service.PersonaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhonghuashishan
 */
@Service
@Slf4j
public class PersonaServiceImpl implements PersonaService {
    @Autowired
    private MembershipFilterDAO membershipFilterDAO;
    /**
     * 查询最大id
     *
     * @param personaFilterCondition
     * @return
     */
    @Override
    public Long queryMaxIdByCondition(PersonaFilterConditionDTO personaFilterCondition) {
        return membershipFilterDAO.getBaseMapper().queryMaxUserId(personaFilterCondition);
    }

    /**
     * 查询最小id
     *
     * @param personaFilterCondition
     * @return
     */
    @Override
    public Long queryMinIdByCondition(PersonaFilterConditionDTO personaFilterCondition) {
        return membershipFilterDAO.getBaseMapper().queryMinUserId(personaFilterCondition);
    }

    /**
     * 根据id范围查询用户id
     *
     * @param personaConditionWithIdRange
     * @return
     */
    @Override
    public List<Long> getAccountIdsByIdRange(PersonaConditionWithIdRange personaConditionWithIdRange) {
        return membershipFilterDAO.getBaseMapper().getAccountIdsByIdRange(personaConditionWithIdRange);
    }

    /**
     * 查询用户数量
     *
     * @param personaFilterCondition
     * @return
     */
    @Override
    public Integer countByCondition(PersonaFilterConditionDTO personaFilterCondition) {
        return membershipFilterDAO.getBaseMapper().countUserIds(personaFilterCondition);
    }

    /**
     * 分页查询用户id
     *
     * @param page
     * @return
     */
    @Override
    public List<Long> getAccountIdsByIdLimit(PersonaConditionPage page) {
        return membershipFilterDAO.getBaseMapper().listAccountIdByPage(page);
    }
}
