package com.ruyuan.eshop.persona.page;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @author zhonghuashishan
 */
@Data
@Builder
public class PersonaConditionPage implements Serializable {

    /**
     * 账号类型
     */
    private Integer accountType;

    /**
     * 会员等级
     */
    private Integer memberLevel;

    /**
     * 会员积分
     */
    private Long memberPoint;

    /**
     * 连续活跃天数
     */
    private Integer activeCount;

    /**
     * 一个月内活跃天数
     */
    private Integer totalActiveCount;

    /**
     * 订单总金额，单位：分
     */
    private Integer totalAmount;

    /**
     * 起始id
     */
    private Long offset;

    /**
     * 结束id
     */
    private Integer limit;
}
