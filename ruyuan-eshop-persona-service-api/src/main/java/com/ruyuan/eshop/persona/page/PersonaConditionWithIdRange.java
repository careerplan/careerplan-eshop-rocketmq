package com.ruyuan.eshop.persona.page;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Max;
import java.io.Serializable;

/**
 * @author zhonghuashishan
 */
@Data
@Builder
public class PersonaConditionWithIdRange implements Serializable {

    /**
     * 会员等级
     */
    private Integer memberLevel;

    /**
     * 会员积分
     */
    private Long memberPoint;

    /**
     * 起始id
     */
    private Long startId;

    /**
     * 结束id
     */
    private Long endId;
}
