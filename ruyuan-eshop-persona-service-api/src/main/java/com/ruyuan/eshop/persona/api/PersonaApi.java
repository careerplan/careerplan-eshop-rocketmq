package com.ruyuan.eshop.persona.api;

import com.ruyuan.eshop.common.core.JsonResult;
import com.ruyuan.eshop.persona.domain.dto.PersonaFilterConditionDTO;
import com.ruyuan.eshop.persona.page.PersonaConditionPage;
import com.ruyuan.eshop.persona.page.PersonaConditionWithIdRange;

import java.util.List;

/**
 * @author zhonghuashishan
 */
public interface PersonaApi {
    /**
     * 获取最大用户Id
     * @param personaFilterCondition
     * @return
     */
    JsonResult<Long> queryMaxIdByCondition(PersonaFilterConditionDTO personaFilterCondition);

    /**
     * 获取最小用户Id
     * @param personaFilterCondition
     * @return
     */
    JsonResult<Long> queryMinIdByCondition(PersonaFilterConditionDTO personaFilterCondition);

    /**
     * 分页获取用户数据
     * @param personaConditionWithIdRange
     * @return
     */
    JsonResult<List<Long>> getAccountIdsByIdRange(PersonaConditionWithIdRange personaConditionWithIdRange);

    /**
     * 根据选人条件获取count值
     * @param personaFilterCondition
     * @return
     */
    JsonResult<Integer> countByCondition(PersonaFilterConditionDTO personaFilterCondition);

    /**
     * 分页获取用户数据
     * @param page
     * @return
     */
    JsonResult<List<Long>> getAccountIdsByIdLimit(PersonaConditionPage page);

}
