/*
 Source Server         : eshop-rocketmq
 Source Server Type    : MySQL
 Source Host           : localhost:3306
 Source Schema         : careerplan_eshop_rocketmq
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for hot_goods_crontab
-- ----------------------------
DROP TABLE IF EXISTS `hot_goods_crontab`;
CREATE TABLE `hot_goods_crontab`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `goods_id` bigint(20) NOT NULL COMMENT '热门商品id',
  `goods_name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '热门商品名',
  `goods_desc` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '热门商品描述',
  `keywords` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '热门商品关键字',
  `crontab_date` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '定时任务日期',
  `portrayal` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户画像',
  `job_shard_suffix` int(4) NOT NULL COMMENT '任务分片后缀',
  `CREATE_USER` int(10) UNSIGNED ZEROFILL NOT NULL DEFAULT 0000000000 COMMENT '创建人',
  `CREATE_TIME` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `UPDATE_USER` int(10) UNSIGNED ZEROFILL NOT NULL DEFAULT 0000000000 COMMENT '更新人',
  `UPDATE_TIME` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 146 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '热门商品任务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for membership_account
-- ----------------------------
DROP TABLE IF EXISTS `membership_account`;
CREATE TABLE `membership_account`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '邮箱',
  `phone_num` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '手机号码',
  `CREATE_USER` int(10) UNSIGNED ZEROFILL NOT NULL DEFAULT 0000000000 COMMENT '创建人',
  `CREATE_TIME` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `UPDATE_USER` int(10) UNSIGNED ZEROFILL NOT NULL DEFAULT 0000000000 COMMENT '更新人',
  `UPDATE_TIME` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '会员中心的用户账号信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for membership_filter
-- ----------------------------
DROP TABLE IF EXISTS `membership_filter`;
CREATE TABLE `membership_filter`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `account_id` bigint(20) NOT NULL COMMENT '用户ID',
  `account_type` tinyint(4) NOT NULL COMMENT '账号类型',
  `membership_level` tinyint(4) NOT NULL COMMENT '会员等级',
  `active_count` tinyint(4) NOT NULL COMMENT '连续活跃天数',
  `total_active_count` tinyint(4) NOT NULL COMMENT '一个月内活跃天数',
  `total_amount` int(11) NOT NULL COMMENT '订单总金额，单位：分',
  `CREATE_USER` int(10) UNSIGNED ZEROFILL NOT NULL DEFAULT 0000000000 COMMENT '创建人',
  `CREATE_TIME` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `UPDATE_USER` int(10) UNSIGNED ZEROFILL NOT NULL DEFAULT 0000000000 COMMENT '更新人',
  `UPDATE_TIME` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '会员中心的会员轨迹表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for membership_point
-- ----------------------------
DROP TABLE IF EXISTS `membership_point`;
CREATE TABLE `membership_point`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `account_id` bigint(20) NOT NULL COMMENT '用户ID',
  `member_level` tinyint(4) NOT NULL COMMENT '会员等级',
  `member_point` bigint(20) NOT NULL COMMENT '会员积分',
  `CREATE_USER` int(10) UNSIGNED ZEROFILL NOT NULL DEFAULT 0000000000 COMMENT '创建人',
  `CREATE_TIME` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `UPDATE_USER` int(10) UNSIGNED ZEROFILL NOT NULL DEFAULT 0000000000 COMMENT '更新人',
  `UPDATE_TIME` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '会员中心的会员积分表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for push_message
-- ----------------------------
DROP TABLE IF EXISTS `push_message`;
CREATE TABLE `push_message`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `main_message` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '消息摘要',
  `push_type` tinyint(4) NOT NULL COMMENT '推送类型：1定时推送，2直接推送',
  `inform_type` tinyint(4) NOT NULL COMMENT '通知类型：1：短信，2：app消息，3：邮箱',
  `message_info` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '消息内容',
  `filter_condition` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '筛选条件',
  `push_start_time` datetime(0) NOT NULL COMMENT '定时发送消息任务开始时间',
  `push_end_time` datetime(0) NOT NULL COMMENT '定时发送消息任务结束时间',
  `send_period_count` int(10) NOT NULL COMMENT '周期内推送次数',
  `CREATE_USER` int(10) UNSIGNED ZEROFILL NOT NULL DEFAULT 0000000000 COMMENT '创建人',
  `CREATE_TIME` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `UPDATE_USER` int(10) UNSIGNED ZEROFILL NOT NULL DEFAULT 0000000000 COMMENT '更新人',
  `UPDATE_TIME` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '优惠券表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for push_message_crontab
-- ----------------------------
DROP TABLE IF EXISTS `push_message_crontab`;
CREATE TABLE `push_message_crontab`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `message_id` bigint(20) NOT NULL COMMENT '消息id',
  `main_message` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '消息摘要',
  `push_type` tinyint(4) NOT NULL COMMENT '推送类型：1定时推送，2直接推送',
  `inform_type` tinyint(4) NOT NULL COMMENT '通知类型：1短信，2app通知，3email通知',
  `message_info` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '消息内容',
  `filter_condition` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '筛选条件',
  `crontab_time` datetime(0) NOT NULL COMMENT '定时任务时间',
  `execute_flag` tinyint(4) NOT NULL DEFAULT 0 COMMENT '任务是否执行： 1-是，2-否',
  `CREATE_USER` int(10) UNSIGNED ZEROFILL NOT NULL DEFAULT 0000000000 COMMENT '创建人',
  `CREATE_TIME` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `UPDATE_USER` int(10) UNSIGNED ZEROFILL NOT NULL DEFAULT 0000000000 COMMENT '更新人',
  `UPDATE_TIME` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '消息发送任务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sales_promotion
-- ----------------------------
DROP TABLE IF EXISTS `sales_promotion`;
CREATE TABLE `sales_promotion`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '促销活动名称',
  `start_time` datetime(0) NOT NULL COMMENT '促销活动开始时间',
  `end_time` datetime(0) NOT NULL COMMENT '促销活动结束时间',
  `inform_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '通知类型',
  `remark` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '促销活动说明备注',
  `status` tinyint(4) NOT NULL COMMENT '促销活动的状态，1：启用，2：停用',
  `type` tinyint(4) NOT NULL COMMENT '促销活动类型，1满减，2折扣，3优惠券，4会员积分',
  `rule` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '促销活动的规则',
  `CREATE_USER` int(10) UNSIGNED ZEROFILL NOT NULL DEFAULT 0000000000 COMMENT '创建人',
  `CREATE_TIME` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `UPDATE_USER` int(10) UNSIGNED ZEROFILL NOT NULL DEFAULT 0000000000 COMMENT '更新人',
  `UPDATE_TIME` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 212 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '促销活动表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sales_promotion_coupon
-- ----------------------------
DROP TABLE IF EXISTS `sales_promotion_coupon`;
CREATE TABLE `sales_promotion_coupon`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `coupon_name` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '优惠券名称',
  `coupon_type` tinyint(4) NOT NULL COMMENT '优惠券类型，1：现金券，2：满减券',
  `coupon_rule` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '优惠券规则',
  `activity_start_time` datetime(0) NOT NULL COMMENT '有效期开始时间',
  `activity_end_time` datetime(0) NOT NULL COMMENT '有效期结束时间',
  `coupon_count` bigint(20) NOT NULL COMMENT '优惠券发行数量',
  `coupon_received_count` bigint(20) NOT NULL COMMENT '优惠券已经领取的数量',
  `coupon_receive_type` tinyint(4) NOT NULL COMMENT '优惠券发放方式，1：仅可领取，2：仅可发放，3：可发放可领取',
  `coupon_status` tinyint(4) NOT NULL COMMENT '优惠券状态，1：发放中，2：已发完，3：过期',
  `CREATE_USER` int(10) UNSIGNED ZEROFILL NOT NULL DEFAULT 0000000000 COMMENT '创建人',
  `CREATE_TIME` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `UPDATE_USER` int(10) UNSIGNED ZEROFILL NOT NULL DEFAULT 0000000000 COMMENT '更新人',
  `UPDATE_TIME` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `inform_type` tinyint(4) NOT NULL COMMENT '通知类型：1：短信，2：app消息，3：邮箱',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1484464364072222721 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '优惠券表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sales_promotion_coupon_item
-- ----------------------------
DROP TABLE IF EXISTS `sales_promotion_coupon_item`;
CREATE TABLE `sales_promotion_coupon_item`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `coupon_id` bigint(20) NOT NULL COMMENT '优惠券ID',
  `user_account_id` bigint(20) NOT NULL COMMENT '用户账号ID',
  `coupon_type` tinyint(4) NOT NULL COMMENT '优惠券类型，1：现金券，2：满减券',
  `is_used` tinyint(4) UNSIGNED ZEROFILL NOT NULL DEFAULT 0000 COMMENT '是否使用过这个优惠券，1：使用了，0：未使用',
  `used_time` datetime(0) NULL DEFAULT NULL COMMENT '使用时间',
  `activity_start_time` datetime(0) NOT NULL COMMENT '有效期开始时间',
  `activity_end_time` datetime(0) NOT NULL COMMENT '有效期结束时间',
  `CREATE_USER` int(10) UNSIGNED ZEROFILL NOT NULL DEFAULT 0000000000 COMMENT '创建人',
  `CREATE_TIME` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `UPDATE_USER` int(10) UNSIGNED ZEROFILL NOT NULL DEFAULT 0000000000 COMMENT '更新人',
  `UPDATE_TIME` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 109 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '优惠券领取记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Procedure structure for generate_user
-- ----------------------------
DROP PROCEDURE IF EXISTS `generate_user`;
delimiter ;;
CREATE DEFINER=`root_careerplan`@`%` PROCEDURE `generate_user`(IN `number` int)
BEGIN
		SET AUTOCOMMIT = 0;
    WHILE number > 0 DO
        INSERT INTO careerplan_eshop_rocketmq.membership_account(username, password, email, phone_num, CREATE_USER, CREATE_TIME, UPDATE_USER, UPDATE_TIME)
        VALUES ('test_username', 'test_password', 'test_email@testeamil.com', '15768980000', 1, NOW(), 1, NOW());
				set number=number-1;
		END WHILE ;
    SET AUTOCOMMIT = 1;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
