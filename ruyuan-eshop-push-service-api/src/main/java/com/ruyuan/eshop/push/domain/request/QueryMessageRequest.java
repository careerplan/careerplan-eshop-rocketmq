package com.ruyuan.eshop.push.domain.request;

import lombok.Data;

import java.util.Date;

/**
 * 消息记录查询
 * @author zhonghuashishan
 */
@Data
public class QueryMessageRequest {

    /**
     * 消息摘要（主题）
     */
    private String mainMessage;

    /**
     * 消息内容
     */
    private String message;

    /**
     * 创建时间
     */
    private Date createTime;

}
