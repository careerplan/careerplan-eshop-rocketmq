package com.ruyuan.eshop.push.domain.dto;

import lombok.Data;

/**
 * 消息推送请求
 * @author zhonghuashishan
 */
@Data
public class SendMessageDTO {

    /**
     * 是否发送成功
     */
    private Boolean success;

}
