package com.ruyuan.eshop.push.enums;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author zhonghuashishan
 */
public enum InformTypeEnum {

    /**
     * 短信
     */
    SMS(1,"短信"),

    /**
     * app
     */
    APP(2, "APP"),

    /**
     * 邮箱
     */
    EMAIL(3, "邮箱"),
    ;

    private Integer code;

    private String msg;

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    InformTypeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static Map<Integer, String> toMap() {
        Map<Integer, String> map = new HashMap<>(16);
        for (InformTypeEnum element : InformTypeEnum.values()) {
            map.put(element.getCode(), element.getMsg());
        }
        return map;
    }

    public static InformTypeEnum getByCode(Integer code) {
        for (InformTypeEnum element : InformTypeEnum.values()) {
            if (code.equals(element.getCode())) {
                return element;
            }
        }
        return null;
    }

    public static Set<Integer> allowableValues() {
        Set<Integer> allowableValues = new HashSet<>(values().length);
        for (InformTypeEnum sourceEnum : values()) {
            allowableValues.add(sourceEnum.getCode());
        }
        return allowableValues;
    }
}
