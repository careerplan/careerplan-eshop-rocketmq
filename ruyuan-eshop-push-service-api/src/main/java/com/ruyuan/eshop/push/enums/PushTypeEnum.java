package com.ruyuan.eshop.push.enums;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author zhonghuashishan
 */
public enum PushTypeEnum {

    /**
     * 定时推送
     */
    DELAY(1,"定时推送"),

    /**
     * 实时推送
     */
    INSTANT(2, "实时推送"),
    ;

    private Integer code;

    private String msg;

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    PushTypeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static Map<Integer, String> toMap() {
        Map<Integer, String> map = new HashMap<>(16);
        for (PushTypeEnum element : PushTypeEnum.values()) {
            map.put(element.getCode(), element.getMsg());
        }
        return map;
    }

    public static PushTypeEnum getByCode(Integer code) {
        for (PushTypeEnum element : PushTypeEnum.values()) {
            if (code.equals(element.getCode())) {
                return element;
            }
        }
        return null;
    }

    public static Set<Integer> allowableValues() {
        Set<Integer> allowableValues = new HashSet<>(values().length);
        for (PushTypeEnum sourceEnum : values()) {
            allowableValues.add(sourceEnum.getCode());
        }
        return allowableValues;
    }
}
