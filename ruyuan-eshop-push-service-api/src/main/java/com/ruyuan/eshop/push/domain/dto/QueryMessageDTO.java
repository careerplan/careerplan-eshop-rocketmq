package com.ruyuan.eshop.push.domain.dto;

import lombok.Data;

/**
 * 消息推送请求
 * @author zhonghuashishan
 */
@Data
public class QueryMessageDTO {

    /**
     * 消息类型：1通知，2消息中心，3既是通知也是消息中心
     * 通知类消息，借助第三方平台推送一个消息，不会保存在数据库
     * 消息中心类消息，需要保存到数据库中
     */
    private Integer informType;

    /**
     * 消息主题
     */
    private String mainMessage;

    /**
     * 消息内容
     */
    private String message;
}
