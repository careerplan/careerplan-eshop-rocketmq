package com.ruyuan.eshop.push.domain.request;

import lombok.Data;

/**
 * 消息推送请求
 * @author zhonghuashishan
 */
@Data
public class SendMessageByAccountRequest {

    /**
     * 用户账号id
     */
    private Long accountId;

    /**
     * 通知类型1：短信，2：app消息，3：邮箱
     */
    private Integer informType;

    /**
     * 消息摘要
     */
    private String mainMessage;

    /**
     * 推送消息内容
     */
    private String message;
}
