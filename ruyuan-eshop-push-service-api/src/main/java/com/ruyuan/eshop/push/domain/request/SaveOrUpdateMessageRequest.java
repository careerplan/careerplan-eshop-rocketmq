package com.ruyuan.eshop.push.domain.request;

import com.ruyuan.eshop.membership.domain.dto.MembershipFilterDTO;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.util.Date;

/**
 * 消息推送请求
 * @author zhonghuashishan
 */
@Data
@Builder
public class SaveOrUpdateMessageRequest {

    /**
     * 推送类型：1定时推送，2直接推送
     */
    private Integer pushType;

    /**
     * 消息主题
     */
    private String mainMessage;
    /**
     * 1：短信，2：app消息，3：邮箱
     */
    private Integer informType;
    /**
     * 推送消息内容
     */
    private String message;

    /**
     * 选人条件
     */
    private MembershipFilterDTO membershipFilterDTO;

    /**
     * 定时发送消息任务开始时间
     */
    private Date pushStartTime;

    /**
     * 定时发送任务结束时间
     */
    private Date pushEndTime;

    /**
     * 每个发送周期内的发送次数，以此为依据发送消息
     */
    private Integer sendPeriodCount;

    /**
     * 推送消息创建/修改人
     */
    private Integer createUser;

    @Tolerate
    public SaveOrUpdateMessageRequest() {

    }
}
