package com.ruyuan.eshop.push.domain.dto;

import lombok.Data;

/**
 * 根据用户ID推送消息结果
 * @author zhonghuashishan
 */
@Data
public class SendMessageByAccountDTO {

    /**
     * 推送结果
     */
    private Boolean success;

}
