package com.ruyuan.eshop.push.domain.dto;

import lombok.Data;

/**
 * 创建/修改活动返回结果
 * @author zhonghuashishan
 */
@Data
public class SaveOrUpdateMessageDTO {

    /**
     * 新增/修改是否成功
     */
    private Boolean success;

}
