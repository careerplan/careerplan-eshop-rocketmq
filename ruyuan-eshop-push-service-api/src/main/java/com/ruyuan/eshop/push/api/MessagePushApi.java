package com.ruyuan.eshop.push.api;

import com.ruyuan.eshop.common.core.JsonResult;
import com.ruyuan.eshop.push.domain.dto.QueryMessageDTO;
import com.ruyuan.eshop.push.domain.dto.SaveOrUpdateMessageDTO;
import com.ruyuan.eshop.push.domain.dto.SendMessageDTO;
import com.ruyuan.eshop.push.domain.request.QueryMessageRequest;
import com.ruyuan.eshop.push.domain.request.SaveOrUpdateMessageRequest;
import com.ruyuan.eshop.push.domain.request.SendMessageByAccountRequest;

import java.util.List;

/**
 * 消息推送接口
 * @author zhonghuashishan
 */
public interface MessagePushApi {
    /**
     * 新增/修改消息推送
     * @param saveOrUpdateMessageRequest
     * @return
     */
    JsonResult<SaveOrUpdateMessageDTO> saveOrUpdateMessage(SaveOrUpdateMessageRequest saveOrUpdateMessageRequest);

    /**
     * 根据accountId单点推送消息
     *
     * @param sendMessageByAccountRequest
     * @return
     */
    JsonResult<SendMessageDTO> sendMessageByAccount(SendMessageByAccountRequest sendMessageByAccountRequest);

    /**
     * 查询消息记录
     *
     * @param queryMessageRequest
     * @return
     */
    JsonResult<List<QueryMessageDTO>> queryMessageByCondition(QueryMessageRequest queryMessageRequest);

}
