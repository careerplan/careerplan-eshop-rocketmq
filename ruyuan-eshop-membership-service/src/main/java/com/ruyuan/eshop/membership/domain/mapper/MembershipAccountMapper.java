package com.ruyuan.eshop.membership.domain.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruyuan.eshop.membership.domain.entity.MembershipAccountDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author zhonghuashishan
 */
@Mapper
public interface MembershipAccountMapper extends BaseMapper<MembershipAccountDO> {

    /**
     * 查询最大的用户id
     */
    @Select("SELECT MAX(id) FROM membership_account")
    Long queryMaxUserId();

    /**
     * 根据id范围查询用户
     */
    @Select("SELECT id FROM membership_account WHERE id >= #{startUserId} AND id < #{endUserId}")
    @ResultType(MembershipAccountDO.class)
    List<MembershipAccountDO> queryAccountByIdRange(@Param("startUserId") Long startUserId,
                                                    @Param("endUserId") Long endUserId);
}
