package com.ruyuan.eshop.membership.api.impl;

import com.ruyuan.eshop.common.core.JsonResult;
import com.ruyuan.eshop.common.exception.BaseBizException;
import com.ruyuan.eshop.membership.api.AccountApi;
import com.ruyuan.eshop.membership.domain.dto.MembershipFilterDTO;
import com.ruyuan.eshop.membership.domain.dto.MembershipAccountDTO;
import com.ruyuan.eshop.membership.domain.dto.SaveOrUpdateAccountDTO;
import com.ruyuan.eshop.membership.domain.request.AccountRequest;
import com.ruyuan.eshop.membership.domain.request.SaveOrUpdateAccountRequest;
import com.ruyuan.eshop.membership.service.MembershipAccountService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author zhonghuashishan
 */
@Slf4j
@DubboService(version = "1.0.0", interfaceClass = AccountApi.class, retries = 0)
public class AccountApiImpl implements AccountApi{
    @Autowired
    private MembershipAccountService membershipAccountService;

    @Override
    public JsonResult<SaveOrUpdateAccountDTO> saveOrUpdateAccount(SaveOrUpdateAccountRequest saveOrUpdateAccountRequest) {
        return null;
    }

    @Override
    public JsonResult<MembershipAccountDTO> getAccount(AccountRequest accountRequest) {
        return null;
    }

    @Override
    public JsonResult<List<MembershipAccountDTO>> listAccount() {
        try {
            List<MembershipAccountDTO> accountDTOS = membershipAccountService.listAll();
            return JsonResult.buildSuccess(accountDTOS);
        } catch (BaseBizException e) {
            log.error("biz error: can not get all accounts", e);
            return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            log.error("system error", e);
            return JsonResult.buildError(e.getMessage());
        }
    }

    @Override
    public JsonResult<Long> queryMaxUserId() {
        try {
            Long maxUserId = membershipAccountService.queryMaxUserId();
            return JsonResult.buildSuccess(maxUserId);
        } catch (BaseBizException e) {
            log.error("biz error: can not get all accounts", e);
            return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            log.error("system error", e);
            return JsonResult.buildError(e.getMessage());
        }
    }

    @Override
    public JsonResult<List<MembershipAccountDTO>> queryAccountByIdRange(Long startUserId, Long endUserId) {
        try {
            List<MembershipAccountDTO> membershipAccountDTOS = membershipAccountService.
                    queryAccountByIdRange(startUserId, endUserId);
            return JsonResult.buildSuccess(membershipAccountDTOS);
        } catch (BaseBizException e) {
            log.error("biz error: can not get all accounts", e);
            return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            log.error("system error", e);
            return JsonResult.buildError(e.getMessage());
        }
    }

    @Override
    public JsonResult<List<MembershipAccountDTO>> listAccountByConditions(MembershipFilterDTO membershipFilterDTO) {
        try {
            List<MembershipAccountDTO> dtos = membershipAccountService.listAccountByConditions(membershipFilterDTO);
            return JsonResult.buildSuccess(dtos);
        } catch (BaseBizException e) {
            log.error("biz error: can not get all accounts", e);
            return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            log.error("system error", e);
            return JsonResult.buildError(e.getMessage());
        }
    }
}
