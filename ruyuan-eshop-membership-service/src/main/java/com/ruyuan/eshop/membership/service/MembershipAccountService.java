package com.ruyuan.eshop.membership.service;

import com.ruyuan.eshop.common.core.JsonResult;
import com.ruyuan.eshop.membership.domain.dto.MembershipFilterDTO;
import com.ruyuan.eshop.membership.domain.dto.MembershipAccountDTO;

import java.util.List;

/**
 * @author zhonghuashishan
 */
public interface MembershipAccountService {
    /**
     * 查询所有账户数据
     * @return
     */
    List<MembershipAccountDTO> listAll();

    /**
     * 查询最大的用户id
     */
    Long queryMaxUserId();

    /**
     * 根据id范围查询用户
     */
    List<MembershipAccountDTO> queryAccountByIdRange(Long startUserId, Long endUserId);

    /**
     * 根据条件查询用户
     * @param membershipFilterDTO
     * @return
     */
    List<MembershipAccountDTO> listAccountByConditions(MembershipFilterDTO membershipFilterDTO);

    /**
     * 根据id查询用户
     * @param userAccountId
     * @return
     */
    MembershipAccountDTO getById(Long userAccountId);
}
