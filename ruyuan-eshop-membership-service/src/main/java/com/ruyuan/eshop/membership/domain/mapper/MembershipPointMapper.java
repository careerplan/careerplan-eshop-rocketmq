package com.ruyuan.eshop.membership.domain.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruyuan.eshop.membership.domain.entity.MembershipPointDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zhonghuashishan
 */
@Mapper
public interface MembershipPointMapper extends BaseMapper<MembershipPointDO> {
}
