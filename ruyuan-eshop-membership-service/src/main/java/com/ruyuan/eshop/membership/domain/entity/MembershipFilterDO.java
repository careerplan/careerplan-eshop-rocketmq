package com.ruyuan.eshop.membership.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @author zhonghuashishan
 */
@Data
@TableName("membership_filter")
public class MembershipFilterDO {

    private Long id;

    private Long accountId;

    private Integer accountType;

    private Integer membershipLevel;

    private Integer activeCount;

    private Integer totalActiveCount;

    private Integer totalAmount;

    private Long createUser;

    private Date createTime;

    private Long updateUser;

    private Date updateTime;

}
