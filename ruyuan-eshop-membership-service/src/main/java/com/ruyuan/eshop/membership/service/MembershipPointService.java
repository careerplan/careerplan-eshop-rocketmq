package com.ruyuan.eshop.membership.service;

import com.ruyuan.eshop.membership.domain.dto.MembershipFilterConditionDTO;
import com.ruyuan.eshop.membership.domain.dto.MembershipPointDTO;

import java.util.List;

/**
 * @author zhonghuashishan
 */
public interface MembershipPointService {

    /**
     * 查询画像匹配用户
     *
     * @param conditionDTO
     * @return
     * @author zhonghuashishan
     */
    List<MembershipPointDTO> listMembershipPointByConditions(MembershipFilterConditionDTO conditionDTO);
}
