package com.ruyuan.eshop.membership.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruyuan.eshop.membership.converter.MembershipPointConverter;
import com.ruyuan.eshop.membership.dao.MembershipPointDAO;
import com.ruyuan.eshop.membership.domain.dto.MembershipFilterConditionDTO;
import com.ruyuan.eshop.membership.domain.dto.MembershipPointDTO;
import com.ruyuan.eshop.membership.domain.entity.MembershipPointDO;
import com.ruyuan.eshop.membership.service.MembershipPointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhonghuashishan
 */
@Service
public class MembershipPointServiceImpl implements MembershipPointService {

    @Autowired
    private MembershipPointDAO membershipPointDAO;

    @Autowired
    private MembershipPointConverter membershipPointConverter;

    @Override
    public List<MembershipPointDTO> listMembershipPointByConditions(MembershipFilterConditionDTO conditionDTO) {
        MembershipPointDO membershipPointDO = membershipPointConverter.dtoToEntity(conditionDTO);

        QueryWrapper<MembershipPointDO> wrapper = new QueryWrapper<>();
        wrapper.setEntity(membershipPointDO);

        return membershipPointConverter.listEntityToDTO(membershipPointDAO.list(wrapper));
    }
}
