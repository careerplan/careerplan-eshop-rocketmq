package com.ruyuan.eshop.membership.api.impl;

import com.alibaba.fastjson.JSON;
import com.ruyuan.eshop.common.core.JsonResult;
import com.ruyuan.eshop.common.exception.BaseBizException;
import com.ruyuan.eshop.membership.api.MembershipPointApi;
import com.ruyuan.eshop.membership.domain.dto.MembershipFilterConditionDTO;
import com.ruyuan.eshop.membership.domain.dto.MembershipPointDTO;
import com.ruyuan.eshop.membership.service.MembershipPointService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author zhonghuashishan
 */
@Slf4j
@DubboService(version = "1.0.0", interfaceClass = MembershipPointApi.class, retries = 0)
public class MembershipPointApiImpl implements MembershipPointApi {

    @Autowired
    private MembershipPointService membershipPointService;

    @Override
    public JsonResult<List<MembershipPointDTO>> listMembershipPointByConditions(MembershipFilterConditionDTO conditionDTO) {
        try {
            List<MembershipPointDTO> dtos = membershipPointService.listMembershipPointByConditions(conditionDTO);
            log.info("查询到用户积分数据, MembershipPointDTO: {}", dtos);
            return JsonResult.buildSuccess(dtos);
        } catch (BaseBizException e) {
            log.error("biz error: request={}", JSON.toJSONString(conditionDTO), e);
            return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            log.error("system error: request={}", JSON.toJSONString(conditionDTO), e);
            return JsonResult.buildError(e.getMessage());
        }
    }
}
