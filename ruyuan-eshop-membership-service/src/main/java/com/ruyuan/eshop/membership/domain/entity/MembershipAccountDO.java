package com.ruyuan.eshop.membership.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @author zhonghuashishan
 */
@Data
@TableName("membership_account")
public class MembershipAccountDO {
    private Long id;

    private String username;

    private String password;

    private String email;

    private String phoneNum;

    private Integer createUser;

    private Date createTime;

    private Integer updateUser;

    private Date updateTime;
}