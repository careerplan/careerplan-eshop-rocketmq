package com.ruyuan.eshop.membership.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @author zhonghuashishan
 */
@Data
@TableName("membership_point")
public class MembershipPointDO {
    private Long id;

    private Long accountId;

    private Integer memberLevel;

    private Long memberPoint;

    private Long createUser;

    private Date createTime;

    private Long updateUser;

    private Date updateTime;
}
