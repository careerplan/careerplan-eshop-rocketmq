package com.ruyuan.eshop.membership.converter;

import com.ruyuan.eshop.membership.domain.dto.MembershipAccountDTO;
import com.ruyuan.eshop.membership.domain.entity.MembershipAccountDO;
import com.ruyuan.eshop.membership.domain.entity.MembershipPointDO;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author zhonghuashishan
 */
@Mapper(componentModel = "spring")
public interface MembershipAccountConverter {
    /**
     * 批量转AccountDO为DTO
     * @param entity
     * @return
     */
    List<MembershipAccountDTO> listEntityToDTO(List<MembershipAccountDO> entity);

    MembershipAccountDTO entityToDTO(MembershipAccountDO byId);
}
