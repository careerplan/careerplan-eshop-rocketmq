package com.ruyuan.eshop.membership.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruyuan.eshop.membership.converter.MembershipAccountConverter;
import com.ruyuan.eshop.membership.dao.MembershipAccountDAO;
import com.ruyuan.eshop.membership.dao.MembershipFilterDAO;
import com.ruyuan.eshop.membership.domain.dto.MembershipFilterDTO;
import com.ruyuan.eshop.membership.domain.dto.MembershipAccountDTO;
import com.ruyuan.eshop.membership.domain.entity.MembershipAccountDO;
import com.ruyuan.eshop.membership.domain.entity.MembershipFilterDO;
import com.ruyuan.eshop.membership.service.MembershipAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author zhonghuashishan
 */
@Service
public class MembershipAccountServiceImpl implements MembershipAccountService {
    /**
     * 账号信息DAO
     */
    @Autowired
    private MembershipAccountDAO membershipAccountDAO;

    @Resource
    private MembershipAccountConverter membershipAccountConverter;

    @Autowired
    private MembershipFilterDAO membershipFilterDAO;

    /**
     * 查询所有用户信息
     * @return
     */
    @Override
    public List<MembershipAccountDTO> listAll() {
        return membershipAccountConverter.listEntityToDTO(membershipAccountDAO.list());
    }

    /**
     * 查询最大的用户id
     */
    @Override
    public Long queryMaxUserId() {
        return membershipAccountDAO.getBaseMapper().queryMaxUserId();
    }

    /**
     * 根据id范围查询用户
     */
    @Override
    public List<MembershipAccountDTO> queryAccountByIdRange(Long startUserId, Long endUserId) {
        List<MembershipAccountDO> membershipAccountDOS = membershipAccountDAO.getBaseMapper()
                .queryAccountByIdRange(startUserId, endUserId);
        return membershipAccountConverter.listEntityToDTO(membershipAccountDOS);
    }

    /**
     * 根据条件查询用户
     * @param membershipFilterDTO
     * @return
     */
    @Override
    public List<MembershipAccountDTO> listAccountByConditions(MembershipFilterDTO membershipFilterDTO) {
        // 构造出查询条件
        LambdaQueryWrapper<MembershipFilterDO> queryWrapper = buildQueryWrapper(membershipFilterDTO);
        // 查询符合条件的用户
        List<MembershipFilterDO> membershipFilterDOs = membershipFilterDAO.list(queryWrapper);

        // 从筛选出的记录中依次拿到accountId，去查询用户数据
        return membershipFilterDOs.stream().map(membershipFilterDO -> {
            Long accountId = membershipFilterDO.getAccountId();
            return membershipAccountConverter.entityToDTO(membershipAccountDAO.getById(accountId));
        }).collect(Collectors.toList());
    }

    /**
     * 根据id查询用户
     *
     * @param userAccountId
     * @return
     */
    @Override
    public MembershipAccountDTO getById(Long userAccountId) {
        return membershipAccountConverter.entityToDTO(membershipAccountDAO.getById(userAccountId));
    }

    /**
     * 构造查询条件
     * @param membershipFilterDTO
     * @return
     */
    private LambdaQueryWrapper<MembershipFilterDO> buildQueryWrapper(MembershipFilterDTO membershipFilterDTO) {
        LambdaQueryWrapper<MembershipFilterDO> queryWrapper = Wrappers.lambdaQuery();
        return queryWrapper
                .and(Objects.nonNull(membershipFilterDTO.getAccountType()),
                        wrapper -> wrapper.eq(MembershipFilterDO::getAccountType, membershipFilterDTO.getAccountType()))
                .and(Objects.nonNull(membershipFilterDTO.getActiveCount()),
                        wrapper -> wrapper.ge(MembershipFilterDO::getActiveCount, membershipFilterDTO.getActiveCount()))
                .and(Objects.nonNull(membershipFilterDTO.getMembershipLevel()),
                        wrapper -> wrapper.ge(MembershipFilterDO::getMembershipLevel, membershipFilterDTO.getMembershipLevel()))
                .and(Objects.nonNull(membershipFilterDTO.getTotalActiveCount()),
                        wrapper -> wrapper.ge(MembershipFilterDO::getTotalActiveCount, membershipFilterDTO.getTotalActiveCount()))
                .and(Objects.nonNull(membershipFilterDTO.getTotalActiveCount()),
                        wrapper -> wrapper.ge(MembershipFilterDO::getTotalAmount, membershipFilterDTO.getTotalAmount()));
    }
}
