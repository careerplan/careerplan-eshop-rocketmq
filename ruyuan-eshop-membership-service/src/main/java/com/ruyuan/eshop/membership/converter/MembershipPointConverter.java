package com.ruyuan.eshop.membership.converter;

import com.ruyuan.eshop.membership.domain.dto.MembershipFilterConditionDTO;
import com.ruyuan.eshop.membership.domain.dto.MembershipPointDTO;
import com.ruyuan.eshop.membership.domain.entity.MembershipPointDO;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author zhonghuashishan
 */
@Mapper(componentModel = "spring")
public interface MembershipPointConverter {

    /**
     * 过滤条件转换至DO
     * @param dto
     * @return
     */
    MembershipPointDO dtoToEntity(MembershipFilterConditionDTO dto);

    /**
     * DO转DTO
     * @param entity
     * @return
     */
    List<MembershipPointDTO> listEntityToDTO(List<MembershipPointDO> entity);

}
