package com.ruyuan.eshop.membership.dao;

import com.ruyuan.eshop.common.dao.BaseDAO;
import com.ruyuan.eshop.membership.domain.entity.MembershipPointDO;
import com.ruyuan.eshop.membership.domain.mapper.MembershipPointMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

/**
 * @author zhonghuashishan
 */
@Slf4j
@Repository
public class MembershipPointDAO extends BaseDAO<MembershipPointMapper, MembershipPointDO> {
}
