package com.ruyuan.eshop.promotion;

import com.ruyuan.eshop.promotion.converter.CouponConverter;
import com.ruyuan.eshop.promotion.dao.SalesPromotionCouponItemDAO;
import com.ruyuan.eshop.promotion.domain.dto.SalesPromotionCouponItemDTO;
import com.ruyuan.eshop.promotion.domain.entity.SalesPromotionCouponItemDO;
import com.ruyuan.eshop.promotion.domain.request.ReceiveCouponRequest;
import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author zhonghuashishan
 */
@SpringBootTest(classes = PromotionApplication.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class CouponItemServiceTest extends TestCase {
    /**
     * 优惠券mapper
     */
    @Autowired
    private SalesPromotionCouponItemDAO salesPromotionCouponItemDAO;

    /**
     * 优惠券对象转换器
     */
    @Autowired
    private CouponConverter couponConverter;

    /**
     * 测试接收优惠券
     */
    @Test
    public void testReceiveCoupon() {
        // 构造请求
        ReceiveCouponRequest request = buildReceiveCouponRequest();
        SalesPromotionCouponItemDO salesPromotionCouponItemDO =
                SalesPromotionCouponItemDO.builder()
                .couponId(request.getCouponId())
                .couponType(request.getCouponType())
                .userAccountId(request.getUserAccountId())
                .activityStartTime(request.getActivityStartTime())
                .activityEndTime(request.getActivityEndTime())
                .createUser(request.getUserAccountId()).build();

        Integer id = salesPromotionCouponItemDAO.receiveCoupon(salesPromotionCouponItemDO);
        Assert.assertNotNull(id);
    }

    /**
     * 构造一个接收优惠券请求
     * @return
     */
    private ReceiveCouponRequest buildReceiveCouponRequest() {
        ReceiveCouponRequest request = ReceiveCouponRequest.builder()
                .couponType(1)
                .couponId(1L)
                .activityStartTime(new Date())
                .activityEndTime(new Date())
                .userAccountId(3L)
                .build();
        return request;
    }

    /**
     * 测试批量保存优惠券
     */
    @Test
    public void testSaveCouponBatch() {
        List<SalesPromotionCouponItemDTO> couponItemDTOS = buildCouponItems();
        List<SalesPromotionCouponItemDO> couponItemDOS = couponItemDTOS.stream().map(couponItemDTO -> {
            SalesPromotionCouponItemDO couponItemDO = couponConverter.
                    convertCouponItemToDO(couponItemDTO);
            return couponItemDO;
        }).collect(Collectors.toList());
        for (SalesPromotionCouponItemDO itemDO:couponItemDOS){
            System.out.println(itemDO.toString());
        }
        Boolean success = salesPromotionCouponItemDAO.saveBatch(couponItemDOS);
        Assert.assertTrue(success);
    }

    private List<SalesPromotionCouponItemDTO> buildCouponItems(){
        List<SalesPromotionCouponItemDTO> list = new ArrayList<>();
        for(int i = 2; i<10; i++) {
            SalesPromotionCouponItemDTO salesPromotionCouponItemDTO =
                    SalesPromotionCouponItemDTO.builder()
                            .couponId(new Long(i))
                            .couponType(1)
                            .userAccountId(new Long(i))
                            .activityStartTime(new Date())
                            .activityEndTime(new Date())
                            .createUser(0L)
                            .build();
            list.add(salesPromotionCouponItemDTO);
        }
        return list;
    }

    /**
     * 测试根据账号id以及优惠券id查询是否存在优惠券
     */
    @Test
    public void testSelectByAccountIdAndCouponId() {
        Long id = salesPromotionCouponItemDAO.selectIdByAccountIdAndCouponId(1L, 1L);
        Assert.assertNotNull(id);
    }
}