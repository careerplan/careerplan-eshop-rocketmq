package com.ruyuan.eshop.promotion;

import com.alibaba.fastjson.JSON;
import com.ruyuan.eshop.common.core.JsonResult;
import com.ruyuan.eshop.common.utils.JsonUtil;
import com.ruyuan.eshop.promotion.domain.dto.SaveOrUpdatePromotionDTO;
import com.ruyuan.eshop.promotion.domain.request.SaveOrUpdatePromotionRequest;
import com.ruyuan.eshop.promotion.service.PromotionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static jdk.nashorn.internal.runtime.regexp.joni.Config.log;

/**
 * @author zhonghuashishan
 */
@SpringBootTest(classes = PromotionApplication.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class PromotionServiceTest {

    @Autowired
    private PromotionService promotionService;

    /**
     * 创建促销活动
     */
    @Test
    public void saveOrUpdateCoupon() {
        // 构造请求
        SaveOrUpdatePromotionRequest request = buildSavePromotionRequest();
        String json = JsonUtil.object2Json(request);
        log.println(json);

        // 执行请求
        SaveOrUpdatePromotionDTO dto = promotionService.saveOrUpdatePromotion(request);
        JsonResult<SaveOrUpdatePromotionDTO> jsonResult = JsonResult.buildSuccess(dto);

        System.out.println(JSON.toJSONString(jsonResult));
    }

    private SaveOrUpdatePromotionRequest buildSavePromotionRequest() {
        Calendar calendar = Calendar.getInstance();
        Date startTime = calendar.getTime();
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        Date endTime = calendar.getTime();

        SaveOrUpdatePromotionRequest.PromotionRulesValue rule = buildPromotionRulesValue();

        SaveOrUpdatePromotionRequest request = new SaveOrUpdatePromotionRequest();
        request.setName("促销活动");
        request.setStartTime(startTime);
        request.setEndTime(endTime);
        request.setRemark("促销活动说明");
        request.setStatus(1);
        request.setType(1);
        request.setRule(rule);
        return request;
    }

    private SaveOrUpdatePromotionRequest.PromotionRulesValue buildPromotionRulesValue() {
        Map<String, String> value = new HashMap<>();
        value.put("满减", "200,30");

        SaveOrUpdatePromotionRequest.PromotionRulesValue promotionRulesValue =
                new SaveOrUpdatePromotionRequest.PromotionRulesValue();
        promotionRulesValue.setKey("满减");
        promotionRulesValue.setValue(value);

        return promotionRulesValue;
    }

}
