package com.ruyuan.eshop.promotion;

import com.alibaba.fastjson.JSON;
import com.ruyuan.eshop.common.core.JsonResult;
import com.ruyuan.eshop.common.enums.CouponSendTypeEnum;
import com.ruyuan.eshop.common.utils.DateUtil;
import com.ruyuan.eshop.promotion.domain.dto.ReceiveCouponDTO;
import com.ruyuan.eshop.promotion.domain.dto.SaveOrUpdateCouponDTO;
import com.ruyuan.eshop.promotion.domain.dto.SendCouponDTO;
import com.ruyuan.eshop.promotion.domain.request.ReceiveCouponRequest;
import com.ruyuan.eshop.promotion.domain.request.SaveOrUpdateCouponRequest;
import com.ruyuan.eshop.promotion.domain.request.SendCouponRequest;
import com.ruyuan.eshop.promotion.enums.CouponTypeEnum;
import com.ruyuan.eshop.promotion.service.CouponService;
import com.ruyuan.eshop.push.enums.InformTypeEnum;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author zhonghuashishan
 */
@SpringBootTest(classes = PromotionApplication.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class PromotionCouponServiceTest {

    @Autowired
    private CouponService couponService;

    /**
     * 创建优惠券
     */
    @Test
    public void saveOrUpdateCoupon() {
        // 构造请求
        SaveOrUpdateCouponRequest request = buildSaveCouponRequest();

        // 执行请求
        SaveOrUpdateCouponDTO dto = couponService.saveOrUpdateCoupon(request);
        JsonResult<SaveOrUpdateCouponDTO> jsonResult = JsonResult.buildSuccess(dto);

        System.out.println(JSON.toJSONString(jsonResult));
    }

    private SaveOrUpdateCouponRequest buildSaveCouponRequest() {
        SaveOrUpdateCouponRequest request = SaveOrUpdateCouponRequest.builder()
                .couponName("测试系统发放优惠券")
                .couponRule("测试优惠券规则")
                .informType(InformTypeEnum.APP.getCode())
                .activityStartTime(new Date())
                .activityEndTime(DateUtil.convertDate(LocalDateTime.now().plusDays(5)))
                .couponCount(10)
                .couponReceivedCount(10)
                .couponReceiveType(CouponSendTypeEnum.PLATFORM_SEND.getCode())
                .couponType(CouponTypeEnum.DISCOUNT.getCode())
                .couponStatus(1)
                .createUser(1)
                .build();
        return request;
    }

    /**
     * 领取优惠券
     */
    @Test
    public void receiveCoupon() {
        // 构造请求
        ReceiveCouponRequest request = buildReceiveCouponRequest();

        // 执行请求
        ReceiveCouponDTO dto = couponService.receiveCoupon(request);
        JsonResult<ReceiveCouponDTO> jsonResult = JsonResult.buildSuccess(dto);

        System.out.println(JSON.toJSONString(jsonResult));
    }

    private ReceiveCouponRequest buildReceiveCouponRequest() {
        ReceiveCouponRequest request = ReceiveCouponRequest.builder()
                .couponId(1L)
                .userAccountId(1L)
                .build();
        return request;
    }

  /*  *//**
     * 根据选人条件发放优惠券
     *//*
    @Test
    public void sendCouponByConditions() {
        // 构造请求
        SendCouponRequest request = buildSendCouponRequest();

        // 执行请求
        SendCouponDTO dto = couponService.sendCouponByConditions(request);
        JsonResult<SendCouponDTO> jsonResult = JsonResult.buildSuccess(dto);

        System.out.println(JSON.toJSONString(jsonResult));
    }*/

    private SendCouponRequest buildSendCouponRequest() {
        SendCouponRequest request = SendCouponRequest.builder()
                .couponName("测试发放需要领取的优惠券")
                .couponType(CouponTypeEnum.DISCOUNT.getCode())
                .couponRule("测试优惠券规则")
                .couponCount(100)
                .activityEndTime(new Date())
                .activityStartTime(new Date())
                .activeUrl("http://www.ruyuan2020.com")
                .pushType(1)
                .informType(1)
                .membershipFilterDTO(null)
                .build();
        return request;
    }

    @Test
    public void receiveCouponAvailable(){
        Long accountId = 4L;
        JsonResult result = couponService.receiveCouponAvailable(accountId);
        Assert.assertTrue(result.getSuccess());
    }


}
