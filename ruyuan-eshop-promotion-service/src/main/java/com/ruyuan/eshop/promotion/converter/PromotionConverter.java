package com.ruyuan.eshop.promotion.converter;

import com.ruyuan.eshop.promotion.domain.entity.SalesPromotionDO;
import com.ruyuan.eshop.promotion.domain.request.SaveOrUpdatePromotionRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * @author zhonghuashishan
 */
@Mapper(componentModel = "spring")
public interface PromotionConverter {

    /**
     * 转换对象
     * @param request 对象
     * @return 对象
     */
    @Mapping(target = "rule", ignore = true)
    SalesPromotionDO convertPromotionDO(SaveOrUpdatePromotionRequest request);

}
