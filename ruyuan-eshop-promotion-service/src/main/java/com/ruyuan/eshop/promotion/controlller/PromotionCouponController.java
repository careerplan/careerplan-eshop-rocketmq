package com.ruyuan.eshop.promotion.controlller;

import com.alibaba.fastjson.JSON;
import com.ruyuan.eshop.common.core.JsonResult;
import com.ruyuan.eshop.common.exception.BaseBizException;
import com.ruyuan.eshop.promotion.domain.dto.ReceiveCouponDTO;
import com.ruyuan.eshop.promotion.domain.dto.SaveOrUpdatePromotionDTO;
import com.ruyuan.eshop.promotion.domain.request.ReceiveCouponRequest;
import com.ruyuan.eshop.promotion.domain.request.SaveOrUpdateCouponRequest;
import com.ruyuan.eshop.promotion.domain.dto.SaveOrUpdateCouponDTO;
import com.ruyuan.eshop.promotion.domain.dto.SendCouponDTO;
import com.ruyuan.eshop.promotion.domain.request.SendCouponRequest;
import com.ruyuan.eshop.promotion.service.CouponItemService;
import com.ruyuan.eshop.promotion.service.CouponService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 发起活动controller
 *
 * @author zhonghuashishan
 */
@Slf4j
@RestController
@RequestMapping("/careerplan/promotion/coupon")
public class PromotionCouponController {

    /**
     * 优惠活动service
     */
    @Autowired
    private CouponService couponService;

    /**
     * 优惠券service
     */
    @Autowired
    private CouponItemService couponItemService;

    /**
     * 新增一个优惠券活动
     * @param request
     * @return
     */
    @PostMapping
    public JsonResult<SaveOrUpdateCouponDTO> saveOrUpdateCoupon(@RequestBody SaveOrUpdateCouponRequest request){
        try {
            log.info("新增一条优惠券:{}", JSON.toJSONString(request));
            SaveOrUpdateCouponDTO dto = couponService.saveOrUpdateCoupon(request);
            return JsonResult.buildSuccess(dto);
        } catch (
                BaseBizException e) {
            log.error("biz error: request={}", JSON.toJSONString(request), e);
            return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            log.error("system error: request={}", JSON.toJSONString(request), e);
            return JsonResult.buildError(e.getMessage());
        }
    }

    @RequestMapping("/send")
    public JsonResult<SendCouponDTO> sendCouponByConditions(@RequestBody SendCouponRequest request){
        try {
            log.info("发送优惠券给指定用户群体:{}", JSON.toJSONString(request));
            SendCouponDTO dto = couponService.sendCouponByConditions(request);
            return JsonResult.buildSuccess(dto);
        } catch (
                BaseBizException e) {
            log.error("biz error: request={}", JSON.toJSONString(request), e);
            return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            log.error("system error: request={}", JSON.toJSONString(request), e);
            return JsonResult.buildError(e.getMessage());
        }
    }

    @RequestMapping("/receive")
    public JsonResult<ReceiveCouponDTO> receiveCoupon(@RequestBody ReceiveCouponRequest request){
        try {
            log.info("领取优惠券:{}", JSON.toJSONString(request));
            ReceiveCouponDTO dto = couponService.receiveCoupon(request);
            return JsonResult.buildSuccess(dto);
        } catch (
                BaseBizException e) {
            log.error("biz error: request={}", JSON.toJSONString(request), e);
            return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            log.error("system error: request={}", JSON.toJSONString(request), e);
            return JsonResult.buildError(e.getMessage());
        }
    }

}
