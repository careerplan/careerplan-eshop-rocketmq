package com.ruyuan.eshop.promotion.api.impl;

import com.alibaba.fastjson.JSON;
import com.ruyuan.eshop.common.core.JsonResult;
import com.ruyuan.eshop.common.exception.BaseBizException;
import com.ruyuan.eshop.promotion.api.PromotionCouponApi;
import com.ruyuan.eshop.promotion.domain.dto.ReceiveCouponDTO;
import com.ruyuan.eshop.promotion.domain.dto.SaveOrUpdateCouponDTO;
import com.ruyuan.eshop.promotion.domain.dto.SendCouponDTO;
import com.ruyuan.eshop.promotion.domain.request.ReceiveCouponRequest;
import com.ruyuan.eshop.promotion.domain.request.SaveOrUpdateCouponRequest;
import com.ruyuan.eshop.promotion.domain.request.SendCouponRequest;
import com.ruyuan.eshop.promotion.service.CouponService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author zhonghuashishan
 */
@Slf4j
@DubboService(version = "1.0.0", interfaceClass = PromotionCouponApi.class, retries = 0)
public class PromotionCouponApiImpl implements PromotionCouponApi {

    @Autowired
    private CouponService couponService;

    @Override
    public JsonResult<SaveOrUpdateCouponDTO> saveOrUpdateCoupon(SaveOrUpdateCouponRequest saveOrUpdateCouponRequest) {
        try {
            SaveOrUpdateCouponDTO dto = couponService.saveOrUpdateCoupon(saveOrUpdateCouponRequest);
            return JsonResult.buildSuccess(dto);
        } catch (BaseBizException e) {
            log.error("biz error: request={}", JSON.toJSONString(saveOrUpdateCouponRequest), e);
            return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            log.error("system error: request={}", JSON.toJSONString(saveOrUpdateCouponRequest), e);
            return JsonResult.buildError(e.getMessage());
        }
    }

    @Override
    public JsonResult<ReceiveCouponDTO> receiveCoupon(ReceiveCouponRequest receiveCouponRequest) {
        try {
            ReceiveCouponDTO dto = couponService.receiveCoupon(receiveCouponRequest);
            return JsonResult.buildSuccess(dto);
        } catch (BaseBizException e) {
            log.error("biz error: request={}", JSON.toJSONString(receiveCouponRequest), e);
            return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            log.error("system error: request={}", JSON.toJSONString(receiveCouponRequest), e);
            return JsonResult.buildError(e.getMessage());
        }
    }

    /**
     * 领取可以领取的优惠券
     * 在用户登录成功以后，进入优惠券中心，或者活动中心领取当前可领的所有优惠券
     *
     * @param account 用户id
     * @return
     */
    @Override
    public JsonResult<Boolean> receiveCouponAvailable(Long account) {
        try {
            return couponService.receiveCouponAvailable(account);
        } catch (BaseBizException e) {
            log.error("biz error: ", e);
            return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            log.error("system error: ", e);
            return JsonResult.buildError(e.getMessage());
        }
    }

    @Override
    public JsonResult<SendCouponDTO> sendCoupon(SendCouponRequest sendCouponRequest) {
        try {
            SendCouponDTO dto = couponService.sendCoupon(sendCouponRequest);
            return JsonResult.buildSuccess(dto);
        } catch (BaseBizException e) {
            log.error("biz error: request={}", JSON.toJSONString(sendCouponRequest), e);
            return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            log.error("system error: request={}", JSON.toJSONString(sendCouponRequest), e);
            return JsonResult.buildError(e.getMessage());
        }
    }

    @Override
    public JsonResult<SendCouponDTO> sendCouponByConditions(SendCouponRequest sendCouponRequest) {
        try {
            SendCouponDTO dto = couponService.sendCouponByConditions(sendCouponRequest);
            return JsonResult.buildSuccess(dto);
        } catch (BaseBizException e) {
            log.error("biz error: request={}", JSON.toJSONString(sendCouponRequest), e);
            return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            log.error("system error: request={}", JSON.toJSONString(sendCouponRequest), e);
            return JsonResult.buildError(e.getMessage());
        }
    }
}
