package com.ruyuan.eshop.promotion.config;

import com.ruyuan.eshop.common.concurrent.SafeThreadPool;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhonghuashishan
 */
@Configuration
public class ThreadPoolConfig {

    /**
     * 发送消息共用的线程池
     * 线程池名字、线程名字：sharedThreadPool
     * 最多允许多少线程同时执行任务：30
     */
    @Bean("sharedSendMsgThreadPool")
    public SafeThreadPool sharedSendMsgThreadPool() {
        return new SafeThreadPool("sharedSendMsgThreadPool", 30);
    }
}
