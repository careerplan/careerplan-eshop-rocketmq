package com.ruyuan.eshop.promotion.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.util.Date;

/**
 * 优惠券item实体类DO
 *
 * @author zhonghuashishan
 */
@Data
@TableName("sales_promotion_coupon_item")
@Builder
public class SalesPromotionCouponItemDO {

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 优惠券id
     */
    private Long couponId;

    /**
     * 优惠券类型：1：现金券，2：满减券
     */
    private Integer couponType;

    /**
     * 用户id
     */
    private Long userAccountId;

    /**
     * 是否已经使用
     */
    private Integer isUsed;

    /**
     * 使用时间
     */
    private Date usedTime;

    /**
     * 有效期开始时间
     */
    private Date activityStartTime;

    /**
     * 有效期开始时间
     */
    private Date activityEndTime;

    /**
     * 创建人
     */
    private Long createUser;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新人
     */
    private Long updateUser;

    /**
     * 更新时间
     */
    private Date updateTime;

    @Tolerate
    public SalesPromotionCouponItemDO(){

    }
}