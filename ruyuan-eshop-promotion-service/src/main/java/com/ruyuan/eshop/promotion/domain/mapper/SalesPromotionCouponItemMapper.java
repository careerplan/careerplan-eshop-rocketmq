package com.ruyuan.eshop.promotion.domain.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruyuan.eshop.promotion.domain.entity.SalesPromotionCouponItemDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author zhonghuashishan
 */
@Mapper
public interface SalesPromotionCouponItemMapper extends BaseMapper<SalesPromotionCouponItemDO> {

    /**
     * 根据账号id以及优惠券id查询是否存在优惠券
     * @param accountId
     * @param couponId
     * @return
     */
    Long selectIdByAccountIdAndCouponId(@Param("user_account_id") Long accountId, @Param("coupon_id")Long couponId);
}