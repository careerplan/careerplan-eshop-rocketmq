package com.ruyuan.eshop.promotion.mq.consumer;

import com.ruyuan.eshop.promotion.mq.config.RocketMQProperties;
import com.ruyuan.eshop.promotion.mq.consumer.listener.*;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.exception.MQClientException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.ruyuan.eshop.common.constants.RocketMqConstant.*;


/**
 * @author zhonghuashishan
 */
@Configuration
public class ConsumerBeanConfig {

    /**
     * 配置内容对象
     */
    @Autowired
    private RocketMQProperties rocketMQProperties;

    /**
     * 平台发放优惠券领取消费者
     * @param platFormCouponListener
     * @return
     * @throws MQClientException
     */
    @Bean("platformCouponReceiveTopicConsumer")
    public DefaultMQPushConsumer receiveCouponConsumer(PlatFormCouponListener platFormCouponListener) throws MQClientException {
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(PLATFORM_COUPON_SEND_CONSUMER_GROUP);
        consumer.setNamesrvAddr(rocketMQProperties.getNameServer());
        consumer.subscribe(PLATFORM_COUPON_SEND_TOPIC, "*");
        consumer.registerMessageListener(platFormCouponListener);
        consumer.start();
        return consumer;
    }

    @Bean("platFormConditionCouponUserBucketConsumer")
    public DefaultMQPushConsumer platFormConditionCouponUserBucketConsumer(PlatFormConditionCouponUserBucketListener platFormPromotionUserBucketListener) throws MQClientException {
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(PLATFORM_CONDITION_COUPON_SEND_USER_BUCKET_CONSUMER_GROUP);
        consumer.setNamesrvAddr(rocketMQProperties.getNameServer());
        consumer.subscribe(PLATFORM_CONDITION_COUPON_SEND_USER_BUCKET_TOPIC, "*");
        consumer.registerMessageListener(platFormPromotionUserBucketListener);
        consumer.start();
        return consumer;
    }


    @Bean("userLoginedEventConsumer")
    public DefaultMQPushConsumer userLoginedEventConsumer(
            UserLoginedEventListener userLoginedEventListener) throws MQClientException {
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(USER_LOGINED_EVENT_CONSUMER_GROUP);
        consumer.setNamesrvAddr(rocketMQProperties.getNameServer());
        consumer.subscribe(USER_LOGINED_EVENT_TOPIC, "*");
        consumer.registerMessageListener(userLoginedEventListener);
        consumer.start();
        return consumer;
    }

    /**
     * 平台发放优惠券用户桶消费者
     * @param platFormCouponUserBucketListener
     * @return
     * @throws MQClientException
     */
    @Bean("platformCouponUserBucketReceiveTopicConsumer")
    public DefaultMQPushConsumer receiveCouponUserBucketConsumer(@Qualifier("platformCouponUserBucketReceiveTopicConsumer")PlatFormCouponUserBucketListener platFormCouponUserBucketListener) throws MQClientException {
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(PLATFORM_COUPON_SEND_USER_BUCKET_CONSUMER_GROUP);
        consumer.setNamesrvAddr(rocketMQProperties.getNameServer());
        consumer.subscribe(PLATFORM_COUPON_SEND_USER_BUCKET_TOPIC, "*");
        consumer.registerMessageListener(platFormCouponUserBucketListener);
        consumer.start();
        return consumer;
    }

    /**
     * 平台发放促销活动用户桶消费者
     * @param platFormPromotionUserBucketListener
     * @return
     * @throws MQClientException
     */
    @Bean("platformPromotionUserBucketReceiveTopicConsumer")
    public DefaultMQPushConsumer receiveCouponUserBucketConsumer(PlatFormPromotionUserBucketListener platFormPromotionUserBucketListener) throws MQClientException {
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(PLATFORM_PROMOTION_SEND_USER_BUCKET_CONSUMER_GROUP);
        consumer.setNamesrvAddr(rocketMQProperties.getNameServer());
        consumer.subscribe(PLATFORM_PROMOTION_SEND_USER_BUCKET_TOPIC, "*");
        consumer.registerMessageListener(platFormPromotionUserBucketListener);
        consumer.start();
        return consumer;
    }

    /**
     * 优惠活动时间监听器消费者
     * @param salesPromotionCreatedEventListener
     * @return
     * @throws MQClientException
     */
    @Bean("salesPromotionCreatedEventConsumer")
    public DefaultMQPushConsumer salesPromotionCreatedEventListener(SalesPromotionCreatedEventListener salesPromotionCreatedEventListener) throws MQClientException {
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(SALES_PROMOTION_CREATED_EVENT_CONSUMER_GROUP);
        consumer.setNamesrvAddr(rocketMQProperties.getNameServer());
        consumer.subscribe(SALES_PROMOTION_CREATED_EVENT_TOPIC, "*");
        consumer.registerMessageListener(salesPromotionCreatedEventListener);
        consumer.start();
        return consumer;
    }

}
