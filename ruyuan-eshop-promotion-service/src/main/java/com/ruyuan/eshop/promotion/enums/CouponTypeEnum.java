package com.ruyuan.eshop.promotion.enums;

/**
 * @author zhonghuashishan
 */
public enum CouponTypeEnum {

    /**
     * 满减
     */
    REDUCTION(1,"满减"),

    /**
     * 折扣
     */
    DISCOUNT(2, "折扣"),
    ;

    private Integer code;

    private String msg;

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    CouponTypeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }


    public static CouponTypeEnum getByCode(Integer code) {
        for (CouponTypeEnum element : CouponTypeEnum.values()) {
            if (code.equals(element.getCode())) {
                return element;
            }
        }
        return null;
    }

}
