package com.ruyuan.eshop.promotion.service;

import com.ruyuan.eshop.common.core.JsonResult;
import com.ruyuan.eshop.promotion.domain.dto.SaveOrUpdatePromotionDTO;
import com.ruyuan.eshop.promotion.domain.request.SaveOrUpdatePromotionRequest;

/**
 * 优惠活动接口
 *
 * @author zhonghuashishan
 */
public interface PromotionService {
    /**
     * 新增/修改促销活动
     * @param saveOrUpdatePromotionRequest
     * @return
     */
    SaveOrUpdatePromotionDTO saveOrUpdatePromotion(SaveOrUpdatePromotionRequest saveOrUpdatePromotionRequest);

}
