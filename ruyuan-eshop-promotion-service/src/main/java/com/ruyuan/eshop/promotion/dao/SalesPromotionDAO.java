package com.ruyuan.eshop.promotion.dao;

import com.ruyuan.eshop.common.dao.BaseDAO;
import com.ruyuan.eshop.promotion.domain.entity.SalesPromotionDO;
import com.ruyuan.eshop.promotion.domain.mapper.SalesPromotionMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author zhonghuashishan
 */
@Repository
@Slf4j
public class SalesPromotionDAO extends BaseDAO<SalesPromotionMapper, SalesPromotionDO> {

    /**
     * 促销活动mapper
     */
    @Autowired
    private SalesPromotionMapper salesPromotionMapper;

    /**
     * 发布一个促销活动
     * @param salesPromotionDO
     * @return
     */
    public Integer saveOrUpdatePromotion(SalesPromotionDO salesPromotionDO){
        return salesPromotionMapper.insert(salesPromotionDO);
    }

}
