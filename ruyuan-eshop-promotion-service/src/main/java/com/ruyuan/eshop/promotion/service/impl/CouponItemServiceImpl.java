package com.ruyuan.eshop.promotion.service.impl;

import com.ruyuan.eshop.common.core.JsonResult;
import com.ruyuan.eshop.promotion.converter.CouponConverter;
import com.ruyuan.eshop.promotion.dao.SalesPromotionCouponItemDAO;
import com.ruyuan.eshop.promotion.domain.dto.SalesPromotionCouponItemDTO;
import com.ruyuan.eshop.promotion.domain.entity.SalesPromotionCouponItemDO;
import com.ruyuan.eshop.promotion.domain.request.ReceiveCouponRequest;
import com.ruyuan.eshop.promotion.service.CouponItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author zhonghuashishan
 */
@Service
public class CouponItemServiceImpl implements CouponItemService {

    /**
     * 优惠券mapper
     */
    @Autowired
    private SalesPromotionCouponItemDAO salesPromotionCouponItemDAO;

    /**
     * 优惠券对象转换器
     */
    @Autowired
    private CouponConverter couponConverter;

    /**
     * 领取优惠券接口
     * @param receiveCouponRequest
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public JsonResult receiveCoupon(ReceiveCouponRequest receiveCouponRequest) {

        SalesPromotionCouponItemDO couponItemDO = SalesPromotionCouponItemDO.builder()
                .couponId(receiveCouponRequest.getCouponId())
                .couponType(receiveCouponRequest.getCouponType())
                .userAccountId(receiveCouponRequest.getUserAccountId())
                .activityStartTime(receiveCouponRequest.getActivityStartTime())
                .activityEndTime(receiveCouponRequest.getActivityEndTime())
                .createUser(receiveCouponRequest.getUserAccountId()).build();
        Integer id = salesPromotionCouponItemDAO.receiveCoupon(couponItemDO);

        if (null == id){
            return JsonResult.buildSuccess(false);
        }
        return JsonResult.buildSuccess(id);
    }

    /**
     * 批量保存优惠券
     * @param couponItemDTOS
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public JsonResult saveCouponBatch(List<SalesPromotionCouponItemDTO> couponItemDTOS){
        // 批量转换对象为DO对象
        List<SalesPromotionCouponItemDO> couponItemDOS = couponItemDTOS.stream().map(couponItemDTO -> {
            SalesPromotionCouponItemDO couponItemDO = couponConverter.
                    convertCouponItemToDO(couponItemDTO);
            return couponItemDO;
        }).collect(Collectors.toList());
        salesPromotionCouponItemDAO.saveBatch(couponItemDOS);
        return JsonResult.buildSuccess(true);
    }

    /**
     * 根据账号id以及优惠券id查询是否存在优惠券
     * @param accountId
     * @param couponId
     * @return
     */
    @Override
    public JsonResult<Long> selectByAccountIdAndCouponId(Long accountId, Long couponId){
        Long id = salesPromotionCouponItemDAO.selectIdByAccountIdAndCouponId(accountId, couponId);
        if (null != id) {
            return JsonResult.buildSuccess(id);
        }
        return JsonResult.buildError("未查到优惠券信息");
    }
}
