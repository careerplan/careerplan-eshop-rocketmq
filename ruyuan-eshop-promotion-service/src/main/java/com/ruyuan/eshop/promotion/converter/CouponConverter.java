package com.ruyuan.eshop.promotion.converter;

import com.ruyuan.eshop.promotion.domain.dto.SalesPromotionCouponItemDTO;
import com.ruyuan.eshop.promotion.domain.entity.SalesPromotionCouponDO;
import com.ruyuan.eshop.promotion.domain.entity.SalesPromotionCouponItemDO;
import com.ruyuan.eshop.promotion.domain.request.SaveOrUpdateCouponRequest;
import com.ruyuan.eshop.promotion.domain.request.SendCouponRequest;
import org.mapstruct.Mapper;

/**
 * @author zhonghuashishan
 */
@Mapper(componentModel = "spring")
public interface CouponConverter {

    /**
     * 转换对象
     * @param request 对象
     * @return 对象
     */
    SalesPromotionCouponDO convertCouponDO(SaveOrUpdateCouponRequest request);

    /**
     * 转换对象
     * @param sendCouponRequest 对象
     * @return 对象
     */
    SalesPromotionCouponDO convertCouponDO(SendCouponRequest sendCouponRequest);

    /**
     * 转换DTO对象为DO对象
     * @param couponItemDTO
     * @return
     */
    SalesPromotionCouponItemDO convertCouponItemToDO(SalesPromotionCouponItemDTO couponItemDTO);

}
