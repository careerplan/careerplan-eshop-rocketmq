package com.ruyuan.eshop.promotion.api.impl;

import com.alibaba.fastjson.JSON;
import com.ruyuan.eshop.common.core.JsonResult;
import com.ruyuan.eshop.common.exception.BaseBizException;
import com.ruyuan.eshop.promotion.api.PromotionApi;
import com.ruyuan.eshop.promotion.api.PromotionCouponApi;
import com.ruyuan.eshop.promotion.domain.dto.SaveOrUpdateCouponDTO;
import com.ruyuan.eshop.promotion.domain.dto.SaveOrUpdatePromotionDTO;
import com.ruyuan.eshop.promotion.domain.request.SaveOrUpdatePromotionRequest;
import com.ruyuan.eshop.promotion.service.PromotionService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author zhonghuashishan
 */
@Slf4j
@DubboService(version = "1.0.0", interfaceClass = PromotionApi.class, retries = 0)
public class PromotionApiImpl implements PromotionApi{
    @Autowired
    private PromotionService promotionService;

    /**
     * 新增/修改促销活动
     * @param saveOrUpdatePromotionRequest
     * @return
     */
    @Override
    public JsonResult<SaveOrUpdatePromotionDTO> saveOrUpdatePromotion(SaveOrUpdatePromotionRequest saveOrUpdatePromotionRequest){
        try {
            SaveOrUpdatePromotionDTO dto  = promotionService.saveOrUpdatePromotion(saveOrUpdatePromotionRequest);
            return JsonResult.buildSuccess(dto);
        } catch (BaseBizException e) {
            log.error("biz error: request={}", JSON.toJSONString(saveOrUpdatePromotionRequest), e);
            return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            log.error("system error: request={}", JSON.toJSONString(saveOrUpdatePromotionRequest), e);
            return JsonResult.buildError(e.getMessage());
        }
    }
}
