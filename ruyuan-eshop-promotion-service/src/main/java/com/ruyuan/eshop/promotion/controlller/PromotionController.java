package com.ruyuan.eshop.promotion.controlller;

import com.alibaba.fastjson.JSON;
import com.ruyuan.eshop.common.core.JsonResult;
import com.ruyuan.eshop.common.exception.BaseBizException;
import com.ruyuan.eshop.promotion.domain.dto.SaveOrUpdatePromotionDTO;
import com.ruyuan.eshop.promotion.domain.request.SaveOrUpdatePromotionRequest;
import com.ruyuan.eshop.promotion.service.PromotionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 发起活动controller
 *
 * @author zhonghuashishan
 */
@Slf4j
@RestController
@RequestMapping("/careerplan/promotion")
public class PromotionController {

    @Autowired
    private PromotionService promotionService;

    /**
     * 新增一个促销活动
     * @param request
     * @return
     */
    @PostMapping("/send")
    public JsonResult<SaveOrUpdatePromotionDTO> saveOrUpdatePromotion(@RequestBody SaveOrUpdatePromotionRequest request){
        try {
            log.info("新增一个促销活动, SaveOrUpdatePromotionRequest: {}", JSON.toJSONString(request));
            SaveOrUpdatePromotionDTO saveOrUpdatePromotionDTO = promotionService.saveOrUpdatePromotion(request);
            return JsonResult.buildSuccess(saveOrUpdatePromotionDTO);
        } catch (BaseBizException e) {
            log.error("biz error: request={}", JSON.toJSONString(request), e);
            return JsonResult.buildError(e.getErrorCode(), e.getErrorMsg());
        } catch (Exception e) {
            log.error("system error: request={}", JSON.toJSONString(request), e);
            return JsonResult.buildError(e.getMessage());
        }

    }

}
