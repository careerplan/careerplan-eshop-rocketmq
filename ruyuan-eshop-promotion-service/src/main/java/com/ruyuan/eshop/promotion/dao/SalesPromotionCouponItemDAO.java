package com.ruyuan.eshop.promotion.dao;


import com.ruyuan.eshop.common.dao.BaseDAO;
import com.ruyuan.eshop.promotion.domain.entity.SalesPromotionCouponItemDO;

import com.ruyuan.eshop.promotion.domain.mapper.SalesPromotionCouponItemMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author zhonghuashishan
 */
@Repository
@Slf4j
public class SalesPromotionCouponItemDAO extends BaseDAO<SalesPromotionCouponItemMapper, SalesPromotionCouponItemDO> {

    /**
     * 优惠券mapper
     */
    @Autowired
    private SalesPromotionCouponItemMapper salesPromotionCouponItemMapper;

    /**
     * 领取一张优惠券
     * @param salesPromotionCouponItemDO
     * @return
     */
    public Integer receiveCoupon(SalesPromotionCouponItemDO salesPromotionCouponItemDO){
        return salesPromotionCouponItemMapper.insert(salesPromotionCouponItemDO);
    }

    /**
     * 根据账号id以及优惠券id查询是否存在优惠券
     * @param accountId
     * @param couponId
     * @return
     */
    public Long selectIdByAccountIdAndCouponId(Long accountId, Long couponId){
        return salesPromotionCouponItemMapper.selectIdByAccountIdAndCouponId(accountId, couponId);
    }
}
