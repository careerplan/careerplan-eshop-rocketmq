package com.ruyuan.eshop.promotion.mq.event;

import com.ruyuan.eshop.promotion.domain.entity.SalesPromotionDO;
import lombok.Data;

/**
 * 促销活动创建事件
 */
@Data
public class SalesPromotionCreatedEvent {

    private SalesPromotionDO salesPromotion;

}
