package com.ruyuan.eshop.promotion.service;

import com.ruyuan.eshop.common.core.JsonResult;
import com.ruyuan.eshop.promotion.domain.dto.SalesPromotionCouponItemDTO;
import com.ruyuan.eshop.promotion.domain.request.ReceiveCouponRequest;
import com.ruyuan.eshop.promotion.domain.dto.ReceiveCouponDTO;

import java.util.List;

/**
 * 优惠券接口
 * @author zhonghuashishan
 */
public interface CouponItemService {
    /**
     * 领取优惠券接口
     * @param receiveCouponRequest
     * @return
     */
    JsonResult<ReceiveCouponDTO> receiveCoupon(ReceiveCouponRequest receiveCouponRequest);

    /**
     * 批量保存优惠券接口
     * @param couponItemDTOS
     * @return
     */
    JsonResult saveCouponBatch(List<SalesPromotionCouponItemDTO> couponItemDTOS);

    /**
     * 根据账号id以及优惠券id查询是否存在优惠券
     * @param accountId
     * @param couponId
     * @return id
     */
    JsonResult<Long> selectByAccountIdAndCouponId(Long accountId, Long couponId);
}
