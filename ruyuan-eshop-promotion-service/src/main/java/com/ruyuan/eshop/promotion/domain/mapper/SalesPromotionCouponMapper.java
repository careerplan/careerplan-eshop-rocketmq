package com.ruyuan.eshop.promotion.domain.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruyuan.eshop.promotion.domain.entity.SalesPromotionCouponDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.List;

/**
 * @author zhonghuashishan
 */
@Mapper
public interface SalesPromotionCouponMapper extends BaseMapper<SalesPromotionCouponDO> {

    /**
     * 根据id范围查询用户优惠券
     */
    @Select("SELECT coupon_id FROM sales_promotion_coupon_item " +
            "WHERE " +
            "user_account_id = #{userAccountId} " +
            "AND activity_end_time < #{activityEndTime} " +
            "ORDER BY coupon_id " +
            "LIMIT #{offset},#{limit}")
    @ResultType(Long.class)
    List<Long> queryAvailableCouponIds(@Param("userAccountId") Long accountId,
                                     @Param("activityEndTime") Date activityEndTime,
                                       @Param("offset")Integer offset,
                                       @Param("limit")Integer limit);
}