package com.ruyuan.eshop.promotion.enums;

/**
 * @author zhonghuashishan
 */
public enum CouponStatusEnum {

    /**
     * 正常
     */
    NORMAL(1,"正常"),

    /**
     * 已发放完
     */
    USED(2, "已发放完"),

    /**
     * 已过期
     */
    EXPIRED(3, "已过期"),
    ;

    private Integer code;

    private String msg;

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    CouponStatusEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }


    public static CouponStatusEnum getByCode(Integer code) {
        for (CouponStatusEnum element : CouponStatusEnum.values()) {
            if (code.equals(element.getCode())) {
                return element;
            }
        }
        return null;
    }

}
