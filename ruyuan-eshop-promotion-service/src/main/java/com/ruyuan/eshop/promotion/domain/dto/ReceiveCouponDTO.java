package com.ruyuan.eshop.promotion.domain.dto;

import lombok.Data;

/**
 * 领取优惠券返回结果
 * @author zhonghuashishan
 */
@Data
public class ReceiveCouponDTO {

    /**
     * 是否领取成功
     */
    private Boolean success;

    /**
     * 领取失败原因
     */
    private String message;
}
