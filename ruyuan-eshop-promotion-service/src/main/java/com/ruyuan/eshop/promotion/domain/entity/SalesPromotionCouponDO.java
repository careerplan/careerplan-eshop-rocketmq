package com.ruyuan.eshop.promotion.domain.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @author zhonghuashishan
 */
@Data
@TableName("sales_promotion_coupon")
public class SalesPromotionCouponDO {
    private Long id;

    private String couponName;

    private Integer couponType;

    private String couponRule;

    /**
     * 1：短信，2：app消息，3：邮箱
     */
    private Integer informType;

    private Date activityStartTime;

    private Date activityEndTime;

    private Integer couponCount;

    private Integer couponReceivedCount;

    private Integer couponReceiveType;

    private Integer couponStatus;

    private Long createUser;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    private Long updateUser;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
