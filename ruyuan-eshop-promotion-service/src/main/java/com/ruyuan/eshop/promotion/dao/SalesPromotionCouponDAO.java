package com.ruyuan.eshop.promotion.dao;

import com.ruyuan.eshop.common.dao.BaseDAO;
import com.ruyuan.eshop.promotion.domain.entity.SalesPromotionCouponDO;
import com.ruyuan.eshop.promotion.domain.mapper.SalesPromotionCouponMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * @author zhonghuashishan
 */
@Repository
@Slf4j
public class SalesPromotionCouponDAO extends BaseDAO<SalesPromotionCouponMapper, SalesPromotionCouponDO> {

    /**
     * 优惠券活动mapper
     */
    @Autowired
    private SalesPromotionCouponMapper salesPromotionCouponMapper;

    /**
     * 发布一个优惠券活动
     * @param salesPromotionCouponDO
     * @return
     */
    public Integer saveOrUpdateCoupon(SalesPromotionCouponDO salesPromotionCouponDO){
        return salesPromotionCouponMapper.insert(salesPromotionCouponDO);
    }

    /**
     * 根据账号id，查询账号中所有生效的优惠券
     * @param accountId
     * @param activityEndTime
     * @return
     */
    public List<Long> queryAvailableCoupon(Long accountId, Date activityEndTime, Integer offset, Integer limit){
        return salesPromotionCouponMapper.queryAvailableCouponIds(accountId, activityEndTime, offset, limit);
    }
}
