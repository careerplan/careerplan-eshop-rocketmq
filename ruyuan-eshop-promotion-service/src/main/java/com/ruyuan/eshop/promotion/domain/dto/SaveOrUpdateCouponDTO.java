package com.ruyuan.eshop.promotion.domain.dto;

import lombok.Data;

/**
 * 创建/修改优惠券返回结果
 * @author zhonghuashishan
 */
@Data
public class SaveOrUpdateCouponDTO {

    /**
     * 新增/修改是否成功
     */
    private Boolean success;

    /**
     * 优惠券名称
     */
    private String couponName;

    /**
     * 活动规则
     */
   private String rule;

}
