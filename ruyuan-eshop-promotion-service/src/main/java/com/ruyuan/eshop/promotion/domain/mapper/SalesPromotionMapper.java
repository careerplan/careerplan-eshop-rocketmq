package com.ruyuan.eshop.promotion.domain.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruyuan.eshop.promotion.domain.entity.SalesPromotionDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zhonghuashishan
 */
@Mapper
public interface SalesPromotionMapper extends BaseMapper<SalesPromotionDO> {
}