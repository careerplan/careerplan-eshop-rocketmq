package com.ruyuan.eshop.promotion.service;

import com.ruyuan.eshop.common.core.JsonResult;
import com.ruyuan.eshop.promotion.domain.dto.ReceiveCouponDTO;
import com.ruyuan.eshop.promotion.domain.dto.SaveOrUpdateCouponDTO;
import com.ruyuan.eshop.promotion.domain.dto.SendCouponDTO;
import com.ruyuan.eshop.promotion.domain.request.ReceiveCouponRequest;
import com.ruyuan.eshop.promotion.domain.request.SaveOrUpdateCouponRequest;
import com.ruyuan.eshop.promotion.domain.request.SendCouponRequest;

/**
 * 优惠券活动接口
 *
 * @author zhonghuashishan
 */
public interface CouponService {

    /**
     * 新增/修改优惠券
     * @param saveOrUpdateCouponRequest
     * @return
     */
    SaveOrUpdateCouponDTO saveOrUpdateCoupon(SaveOrUpdateCouponRequest saveOrUpdateCouponRequest);

    /**
     * 领取优惠券
     * @param receiveCouponRequest
     * @return
     */
    ReceiveCouponDTO receiveCoupon(ReceiveCouponRequest receiveCouponRequest);

    /**
     * 发放优惠券 发放过程可以直接发送至MQ，异步消费至优惠券item表
     * @param sendCouponRequest
     * @return
     */
    SendCouponDTO sendCoupon(SendCouponRequest sendCouponRequest);

    /**
     * 根据选人条件发放唤回优惠券，
     * 此接口需要调用推送系统的按条件推送消息的接口，
     * 传入选人条件，以及把优惠券+优惠券领取地址封装到一个对象中
     *
     * @param sendCouponRequest
     * @return
     */
    SendCouponDTO sendCouponByConditions(SendCouponRequest sendCouponRequest);

    /**
     * 领取所有有效优惠券
     * @param accountId
     * @return
     */
    JsonResult<Boolean> receiveCouponAvailable(Long accountId);
}
