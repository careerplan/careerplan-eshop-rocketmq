package com.ruyuan.eshop.promotion.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * 优惠活动实体类DO
 * @author zhonghuashishan
 */
@Data
@TableName("sales_promotion")
public class SalesPromotionDO {

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 促销活动名称
     */
    private String name;

    /**
     * 促销活动开始时间
     */
    private Date startTime;

    /**
     * 促销活动结束时间
     */
    private Date endTime;

    /**
     * 促销活动说明备注
     */
    private String remark;

    /**
     * 促销活动状态，1：启用；2：停用
     */
    private Integer status;

    /**
     * 促销活动类型，1满减，2折扣，3优惠券，4会员积分
     */
    private Integer type;

    /**
     * 1：短信，2：app消息，3：邮箱
     */
    private Integer informType;

    /**
     * 促销活动规则
     */
    private String rule;

    private Integer createUser;

    private Date createTime;

    private Integer updateUser;

    private Date updateTime;
}