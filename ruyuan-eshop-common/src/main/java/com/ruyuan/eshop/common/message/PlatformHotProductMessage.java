package com.ruyuan.eshop.common.message;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 平台热门商品推送消息
 *
 * @author zhonghuashishan
 * @version 1.0
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlatformHotProductMessage implements Serializable {

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 商品关键词
     */
    private List<String> keyWords;
    /**
     * 商品描述
     */
    private String goodsDesc;

    /**
     * 用户ID
     */
    private Long accountId;

    private PlatformHotProductMessage(Builder builder) {
        this.goodsName = builder.goodsName;
        this.keyWords = builder.keyWords;
        this.goodsDesc = builder.goodsDesc;
        this.accountId = builder.accountId;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String goodsName;
        private List<String> keyWords;
        private String goodsDesc;
        private Long accountId;

        public Builder goodsName(String goodsName) {
            this.goodsName = goodsName;
            return this;
        }

        public Builder keyWords(List<String> keyWords) {
            this.keyWords = keyWords;
            return this;
        }

        public Builder goodsDesc(String goodsDesc) {
            this.goodsDesc = goodsDesc;
            return this;
        }

        public Builder accountId(Long accountId) {
            this.accountId = accountId;
            return this;
        }

        public PlatformHotProductMessage build() {
            return new PlatformHotProductMessage(this);
        }
    }

}
