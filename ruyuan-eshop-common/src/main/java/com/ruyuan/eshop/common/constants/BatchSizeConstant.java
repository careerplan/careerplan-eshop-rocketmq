package com.ruyuan.eshop.common.constants;

/**
 * @author zhonghuashishan
 */
public class BatchSizeConstant {

    /**
     * 桶的大小
     */
    public static final Integer USER_BUCKET_SIZE = 1000;

    /**
     * mq的batch发送size
     */
    public static final Integer MESSAGE_BATCH_SIZE = 100;
}
