package com.ruyuan.eshop.common.message;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Tolerate;

import java.io.Serializable;

/**
 * 平台促销活动用户桶消息
 *
 * @author zhonghuashishan
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlatformPromotionUserBucketMessage implements Serializable{
    private static final long serialVersionUID = -9027846603047221464L;

    /**
     * 当前桶内起始的用户id（包含）
     */
    private Long startUserId;

    /**
     * 当前桶内结束的userid（不包含）
     */
    private Long endUserId;

    /**
     * 活动id
     */
    private Integer promotionId;
    /**
     * 主题
     */
    private String mainMessage;

    /**
     * 消息内容
     */
    private String message;
    /**
     * 活动类型
     */
    private Integer promotionType;

    /**
     * 消息类型:1短信，2app，3email
     */
    private Integer informType;

    /**
     * 用户id
     */
    private Long userAccountId;
}