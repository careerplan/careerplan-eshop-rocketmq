package com.ruyuan.eshop.common.message;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Tolerate;

import java.io.Serializable;

/**
 * 平台消息发送消息
 *
 * @author zhonghuashishan
 * @version 1.0
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlatformMessagePushMessage implements Serializable {

    /**
     * 主题
     */
    private String mainMessage;

    /**
     * 消息内容
     */
    private String message;

    /**
     * 消息类型
     */
    private Integer informType;

    /**
     * 用户id
     */
    private Long userAccountId;

}