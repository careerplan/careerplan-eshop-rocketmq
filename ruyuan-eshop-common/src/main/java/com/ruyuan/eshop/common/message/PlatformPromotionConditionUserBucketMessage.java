package com.ruyuan.eshop.common.message;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhonghuashishan
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlatformPromotionConditionUserBucketMessage {

    /**
     * 筛选条件：json格式
     */
    String personaFilterCondition;

    /**
     * 分片id
     */
    Integer shardId;

    /**
     * 分片须处理数量
     */
    Integer bucketSize;
}
