package com.ruyuan.eshop.common.constants;

/**
 * @author zhonghuashishan
 */
public class RedisKey {
    public static final String PROMOTION_COUPON_KEY = "promotion_coupon";
    public static final String PROMOTION_CONDITION_COUPON_KEY = "promotion_condition_coupon_key";
    public static final String PROMOTION_COUPON__LOCK_KEY = "promotion_coupon_lock";
    public static final String PROMOTION_USER_RECEIVE_COUPON = "promotion_user_receive_coupon";
    public static final String PROMOTION_COUPON_ID_LIST_LOCK = "promotion_coupon_id_list_lock";
    public static final String PROMOTION_COUPON_ID_LIST = "promotion_coupon_id_list";
    /**
     * 优惠券幂等key
     */
    public static final String COUPON_NX_KEY = "coupon_nx";

    public static final String PROMOTION_CONCURRENCY_KEY = "promotion_Concurrency";
}
