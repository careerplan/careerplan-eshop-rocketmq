package com.ruyuan.eshop.common.constants;

/**
 * 通知类型常量类
 * @author zhonghuashishan
 */
public class InformType {
    /**
     * APP消息推送
     */
    public static final int APP_PUSH_MESSAGE = 0;

    /**
     * 短信通知
     */
    public static final int PHONE_SHORT_MESSAGE = 1;

    /**
     * email通知
     */
    public static final int EMAIL_MESSAGE = 2;


}
