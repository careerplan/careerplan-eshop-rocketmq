package com.ruyuan.eshop.common.enums;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 是/否 枚举值
 * @author zhonghuashishan
 */
public enum YesOrNoEnum {
    /**
     * 是
     */
    YES(1,"是"),
    NO(0, "否"),
    ;

    private Integer code;

    private String msg;

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    YesOrNoEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static Map<Integer, String> toMap() {
        Map<Integer, String> map = new HashMap<>(16);
        for (YesOrNoEnum element : YesOrNoEnum.values()) {
            map.put(element.getCode(), element.getMsg());
        }
        return map;
    }

    public static YesOrNoEnum getByCode(Integer code) {
        for (YesOrNoEnum element : YesOrNoEnum.values()) {
            if (code.equals(element.getCode())) {
                return element;
            }
        }
        return null;
    }

    public static Set<Integer> allowableValues() {
        Set<Integer> allowableValues = new HashSet<>(values().length);
        for (YesOrNoEnum sourceEnum : values()) {
            allowableValues.add(sourceEnum.getCode());
        }
        return allowableValues;
    }
}
