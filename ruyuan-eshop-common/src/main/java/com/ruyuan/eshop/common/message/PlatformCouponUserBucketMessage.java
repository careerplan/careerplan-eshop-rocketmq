package com.ruyuan.eshop.common.message;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bouncycastle.asn1.cms.PasswordRecipientInfo;

import java.io.Serializable;
import java.util.Date;

/**
 * 平台优惠券发放消息
 *
 * @author zhonghuashishan
 * @version 1.0
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlatformCouponUserBucketMessage implements Serializable {

    private static final long serialVersionUID = 2575864833116171389L;

    /**
     * 当前桶内起始的用户id（包含）
     */
    private Long startUserId;

    /**
     * 当前桶内结束的userid（不包含）
     */
    private Long endUserId;

    /**
     * 优惠券编号号
     */
    private Long couponId;

    /**
     * 用户id
     */
    private Long userAccountId;

    /**
     * 优惠券类型 1：现金券，2：满减券
     */
    private Integer couponType;

    /**
     * 消息类型:1短信，2app，3email
     */
    private Integer informType;

    /**
     * 生效开始时间
     */
    private Date activityStartTime;

    /**
     * 生效结束时间
     */
    private Date activityEndTime;
}