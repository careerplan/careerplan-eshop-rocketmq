package com.ruyuan.eshop.common.enums;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author zhonghuashishan
 */
public enum CouponSendTypeEnum {
    // 1：系统发放及自领取，2：仅系统发放，3：仅自领取
    SELF_RECEIVE(1,"仅自领取"),
    PLATFORM_SEND(2, "系统发放"),
    ;

    private Integer code;

    private String msg;

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    CouponSendTypeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static Map<Integer, String> toMap() {
        Map<Integer, String> map = new HashMap<>(16);
        for (CouponSendTypeEnum element : CouponSendTypeEnum.values()) {
            map.put(element.getCode(), element.getMsg());
        }
        return map;
    }

    public static CouponSendTypeEnum getByCode(Integer code) {
        for (CouponSendTypeEnum element : CouponSendTypeEnum.values()) {
            if (code.equals(element.getCode())) {
                return element;
            }
        }
        return null;
    }

    public static Set<Integer> allowableValues() {
        Set<Integer> allowableValues = new HashSet<>(values().length);
        for (CouponSendTypeEnum sourceEnum : values()) {
            allowableValues.add(sourceEnum.getCode());
        }
        return allowableValues;
    }
}
