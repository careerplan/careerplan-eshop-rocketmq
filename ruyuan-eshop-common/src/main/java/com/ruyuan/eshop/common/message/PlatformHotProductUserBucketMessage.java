package com.ruyuan.eshop.common.message;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 热门商品用户桶消息
 *
 * @author zhonghuashishan
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlatformHotProductUserBucketMessage implements Serializable{
    private static final long serialVersionUID = -9027846603047221464L;

    /**
     * 当前桶内起始的用户id（包含）
     */
    private Long startUserId;

    /**
     * 当前桶内结束的userid（不包含）
     */
    private Long endUserId;

    /**
     * 热门商品:json类型
     */
    private String hotGoodsVO;

    /**
     * 用户画像：json类型
     */
    private String personaFilterConditionDTO;
}