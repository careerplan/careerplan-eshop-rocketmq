package com.ruyuan.eshop.common.message;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 平台优惠券发放消息
 *
 * @author zhonghuashishan
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlatformCouponMessage implements Serializable {

    private static final long serialVersionUID = 2575864833116171389L;

    /**
     * 优惠券编号号
     */
    private Long couponId;

    /**
     * 用户id
     */
    private Long userAccountId;

    /**
     * 优惠券类型 1：现金券，2：满减券
     */
    private Integer couponType;

    /**
     * 生效开始时间
     */
    private Date activityStartTime;

    /**
     * 生效结束时间
     */
    private Date activityEndTime;

    private PlatformCouponMessage(Builder builder) {
        this.couponId = builder.couponId;
        this.userAccountId = builder.userAccountId;
        this.couponType = builder.couponType;
        this.activityStartTime = builder.activityStartTime;
        this.activityEndTime = builder.activityEndTime;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Long couponId;
        private Long userAccountId;
        private Integer couponType;
        private Date activityStartTime;
        private Date activityEndTime;

        public Builder couponId(Long couponId) {
            this.couponId = couponId;
            return this;
        }

        public Builder userAccountId(Long userAccountId) {
            this.userAccountId = userAccountId;
            return this;
        }

        public Builder couponType(Integer couponType) {
            this.couponType = couponType;
            return this;
        }

        public Builder activityStartTime(Date activityStartTime) {
            this.activityStartTime = activityStartTime;
            return this;
        }

        public Builder activityEndTime(Date activityEndTime) {
            this.activityEndTime = activityEndTime;
            return this;
        }

        public PlatformCouponMessage build() {
            return new PlatformCouponMessage(this);
        }
    }

}