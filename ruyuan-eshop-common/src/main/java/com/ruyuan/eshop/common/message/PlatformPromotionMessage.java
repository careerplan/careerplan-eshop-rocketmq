package com.ruyuan.eshop.common.message;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Tolerate;

import java.io.Serializable;

/**
 * 平台促销活动消息
 *
 * @author zhonghuashishan
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlatformPromotionMessage implements Serializable{
    /**
     * 活动id
     */
    private Integer promotionId;
    /**
     * 活动类型
     */
    private Integer promotionType;
    /**
     * 主题
     */
    private String mainMessage;

    /**
     * 消息内容
     */
    private String message;
    /**
     * 消息类型
     */
    private Integer informType;
    /**
     * 用户id
     */
    private Long userAccountId;

    public String cacheKey() {
        return "idempotent-control:" + promotionId +":" + userAccountId;
    }

}