package com.ruyuan.eshop.membership.domain.request;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.io.Serializable;

/**
 * 新增/编辑品类请求入参
 *
 * @author zhonghuashishan
 */
@Data
@Builder
public class AccountRequest implements Serializable {
    /**
     * 账号id
     */
    private Integer id;
    /**
     * 账户名称
     */
    private String userName;

    @Tolerate
    public AccountRequest() {

    }
}