package com.ruyuan.eshop.membership.api;

import com.ruyuan.eshop.common.core.JsonResult;
import com.ruyuan.eshop.membership.domain.dto.MembershipFilterDTO;
import com.ruyuan.eshop.membership.domain.dto.MembershipAccountDTO;
import com.ruyuan.eshop.membership.domain.dto.SaveOrUpdateAccountDTO;
import com.ruyuan.eshop.membership.domain.request.AccountRequest;
import com.ruyuan.eshop.membership.domain.request.SaveOrUpdateAccountRequest;

import java.util.List;

/**
 * @author zhonghuashishan
 */
public interface AccountApi {
    /**
     * 新增/修改账户接口
     *
     * @param saveOrUpdateAccountRequest
     * @return
     * @author zhonghuashishan
     */
    JsonResult<SaveOrUpdateAccountDTO> saveOrUpdateAccount(SaveOrUpdateAccountRequest saveOrUpdateAccountRequest);

    /**
     * 查询账户接口
     *
     * @param accountRequest
     * @return
     * @author zhonghuashishan
     */
    JsonResult<MembershipAccountDTO> getAccount(AccountRequest accountRequest);


    /**
     * 查询全部账户接口
     * @return
     */
    JsonResult<List<MembershipAccountDTO>> listAccount();

    /**
     * 查询最大的用户id
     */
    JsonResult<Long> queryMaxUserId();

    /**
     * 根据id范围查询用户
     */
    JsonResult<List<MembershipAccountDTO>> queryAccountByIdRange(Long startUserId, Long endUserId);

    /**
     * 查询符合条件的账户接口
     * @param membershipFilterDTO
     * @return
     */
    JsonResult<List<MembershipAccountDTO>> listAccountByConditions(MembershipFilterDTO membershipFilterDTO);
}
