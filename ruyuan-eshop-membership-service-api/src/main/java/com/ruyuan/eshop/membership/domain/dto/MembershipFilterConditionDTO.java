package com.ruyuan.eshop.membership.domain.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @author zhonghuashishan
 */
@Data
@Builder
public class MembershipFilterConditionDTO implements Serializable {
    private Integer memberLevel;
    private Long memberPoint;
}
