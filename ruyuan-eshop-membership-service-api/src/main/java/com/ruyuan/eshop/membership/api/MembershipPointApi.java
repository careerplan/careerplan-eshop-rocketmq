package com.ruyuan.eshop.membership.api;

import com.ruyuan.eshop.common.core.JsonResult;
import com.ruyuan.eshop.membership.domain.dto.MembershipFilterConditionDTO;
import com.ruyuan.eshop.membership.domain.dto.MembershipPointDTO;

import java.util.List;

/**
 * @author zhonghuashishan
 */
public interface MembershipPointApi {

    /**
     * 查询画像匹配用户
     *
     * @param conditionDTO
     * @return
     * @author zhonghuashishan
     */
    JsonResult<List<MembershipPointDTO>> listMembershipPointByConditions(MembershipFilterConditionDTO conditionDTO);

}
