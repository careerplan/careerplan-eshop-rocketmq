package com.ruyuan.eshop.membership.domain.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zhonghuashishan
 */
@Data
public class MembershipPointDTO implements Serializable {
    private Long id;

    private Long accountId;

    private Integer memberLevel;

    private Long memberPoint;

    private Long createUser;

    private Date createTime;

    private Long updateUser;

    private Date updateTime;
}