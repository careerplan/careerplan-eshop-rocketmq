package com.ruyuan.eshop.membership.domain.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zhonghuashishan
 */
@Data
public class MembershipAccountDTO implements Serializable {
    private Long id;

    private String username;

    private String password;

    private String email;

    private String phoneNum;

    private Integer createUser;

    private Date createTime;

    private Integer updateUser;

    private Date updateTime;
}