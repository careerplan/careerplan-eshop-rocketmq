package com.ruyuan.eshop.membership.domain.request;


import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 新增/编辑账号请求入参
 *
 * @author zhonghuashishan
 * @version 1.0
 */
@Data
public class SaveOrUpdateAccountRequest implements Serializable {

    /**
     * id
     */
    private Long id;

    /**
     * 账户名称
     */
    @NotNull(message = "账户名称[userName]不能为空")
    private String userName;

    /**
     * 账户密码
     */
    @NotNull(message = "账户密码[password]不能为空")
    private String password;

    /**
     * 电子邮箱
     */
    private String email;

    /**
     * 联系电话
     */
    private String phoneNum;

    /**
     * 操作人
     */
    @NotNull(message = "操作人[operateUser]不能为空")
    private Integer operateUser;

}