package com.ruyuan.eshop.membership.mq.event;

import com.ruyuan.eshop.membership.domain.dto.MembershipAccountDTO;
import lombok.Data;

/**
 * 用户登录事件
 */
@Data
public class UserLoginedEvent {

    private MembershipAccountDTO account;

}
