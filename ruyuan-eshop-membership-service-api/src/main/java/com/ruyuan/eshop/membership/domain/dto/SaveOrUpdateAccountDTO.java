package com.ruyuan.eshop.membership.domain.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 新增/修改账号返回结果
 *
 * @author zhonghuashishan
 */
@Data
public class SaveOrUpdateAccountDTO implements Serializable {

    /**
     * 新增/修改是否成功
     */
    private Boolean success;

}