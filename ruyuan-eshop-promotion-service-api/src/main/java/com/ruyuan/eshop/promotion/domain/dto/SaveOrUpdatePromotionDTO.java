package com.ruyuan.eshop.promotion.domain.dto;

import lombok.Data;

/**
 * 创建/修改活动返回结果
 * @author zhonghuashishan
 */
@Data
public class SaveOrUpdatePromotionDTO {

    /**
     * 新增/修改是否成功
     */
    private Boolean success;

    /**
     * 活动类型
     */
    private Integer type;

    /**
     * 活动名称
     */
    private String name;

    /**
     * 活动规则
     */
    private String rule;

    /**
     * 活动创建/修改人
     */
    private Integer createUser;

}
