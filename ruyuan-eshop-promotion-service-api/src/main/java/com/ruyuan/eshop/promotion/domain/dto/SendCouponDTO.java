package com.ruyuan.eshop.promotion.domain.dto;

import lombok.Data;

/**
 * 领取优惠券返回值
 * @author zhonghuashishan
 */
@Data
public class SendCouponDTO {

    /**
     * 发放结果，是否成功
     */
    private Boolean success;

    /**
     * 发放数量
     */
    private Integer sendCount;

    /**
     * 优惠券名称
     */
    private String couponName;

    /**
     * 优惠规则
     */
    private String rule;

}
