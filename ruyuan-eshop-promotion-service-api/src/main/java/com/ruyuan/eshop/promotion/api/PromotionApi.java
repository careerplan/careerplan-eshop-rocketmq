package com.ruyuan.eshop.promotion.api;

import com.ruyuan.eshop.common.core.JsonResult;
import com.ruyuan.eshop.promotion.domain.dto.SaveOrUpdatePromotionDTO;
import com.ruyuan.eshop.promotion.domain.request.SaveOrUpdatePromotionRequest;

/**
 * 促销活动接口
 *
 * @author zhonghuashishan
 */
public interface PromotionApi {

    /**
     * 新增/修改促销活动
     * @param saveOrUpdatePromotionRequest
     * @return
     */
    JsonResult<SaveOrUpdatePromotionDTO> saveOrUpdatePromotion(SaveOrUpdatePromotionRequest saveOrUpdatePromotionRequest);


}

