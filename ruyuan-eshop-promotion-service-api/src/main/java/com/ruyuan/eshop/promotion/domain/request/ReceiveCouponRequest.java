package com.ruyuan.eshop.promotion.domain.request;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.io.Serializable;
import java.util.Date;

/**
 * 领取优惠券请求
 *
 * @author zhonghuashishan
 */
@Data
@Builder
public class ReceiveCouponRequest implements Serializable {

    /**
     * 优惠券ID
     */
    private Long couponId;

    /**
     * 优惠券类型：1：现金券，2：满减券
     */
    private Integer couponType;

    /**
     * 用户账号
     */
    private Long userAccountId;

    /**
     * 优惠规则
     */
    private String couponRule;

    /**
     * 优惠券生效开始时间
     */
    private Date activityStartTime;

    /**
     * 优惠券生效结束时间
     */
    private Date activityEndTime;

    @Tolerate
    public ReceiveCouponRequest() {

    }
}
