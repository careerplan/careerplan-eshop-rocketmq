package com.ruyuan.eshop.promotion.domain.request;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.io.Serializable;
import java.util.Date;

/**
 * 创建/更新优惠券活动请求
 *
 * @author zhonghuashishan
 */
@Data
@Builder
public class SaveOrUpdateCouponRequest implements Serializable {
    /**
     * 优惠券名称
     */
    private String couponName;

    /**
     * 优惠规则
     */
    private String couponRule;

    /**
     * 活动开始时间
     */
    private Date activityStartTime;

    /**
     * 活动结束时间
     */
    private Date activityEndTime;

    /**
     * 1：短信，2：app消息，3：邮箱
     */
    private Integer informType;

    /**
     * 优惠券发放数量
     */
    private Integer couponCount;

    /**
     * 优惠券领取数量
     */
    private Integer couponReceivedCount;

    /**
     * 优惠券发放方式,优惠券发放方式，1：仅自领取，2：仅系统发放
     */
    private Integer couponReceiveType;

    /**
     * 优惠券类型
     */
    private Integer couponType;

    /**
     * 优惠券状态：优惠券状态，1：发放中，2：已发完，3：已过期
     */
    private Integer couponStatus;

    /**
     * 活动创建/修改人
     */
    private Integer createUser;

    @Tolerate
    public SaveOrUpdateCouponRequest() {

    }
}
