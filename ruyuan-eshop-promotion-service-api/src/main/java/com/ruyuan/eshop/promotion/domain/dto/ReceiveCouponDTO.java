package com.ruyuan.eshop.promotion.domain.dto;

import lombok.Data;

/**
 * 领取优惠券返回值
 * @author zhonghuashishan
 */
@Data
public class ReceiveCouponDTO {

    /**
     * 领取结果，是否成功
     */
    private Boolean success;

    /**
     * 优惠券名称
     */
    private String couponName;

    /**
     * 优惠规则
     */
    private String rule;

    /**
     * 领取失败时，返回信息
     */
    private String message;
}
