package com.ruyuan.eshop.promotion.domain.request;

import lombok.Data;

import java.io.Serializable;

/**
 * 领取优惠券请求
 *
 * @author zhonghuashishan
 */
@Data
public class SendCouponByConditionsRequest implements Serializable {
    /**
     * 优惠券ID
     */
    private Long couponId;
}
