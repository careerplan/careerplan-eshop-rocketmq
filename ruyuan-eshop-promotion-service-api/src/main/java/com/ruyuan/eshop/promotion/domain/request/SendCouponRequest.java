package com.ruyuan.eshop.promotion.domain.request;

import com.ruyuan.eshop.membership.domain.dto.MembershipFilterDTO;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.io.Serializable;
import java.util.Date;

/**
 * 发送优惠券请求
 *
 * @author zhonghuashishan
 */
@Data
@Builder
public class SendCouponRequest implements Serializable {
    /**
     * 优惠券名称
     */
    private String couponName;

    /**
     * 优惠券类型
     */
    private Integer couponType;

    /**
     * 优惠规则
     */
    private String couponRule;

    /**
     * 活动开始时间
     */
    private Date activityStartTime;

    /**
     * 活动结束时间
     */
    private Date activityEndTime;

    /**
     * 优惠券发放数量
     */
    private Integer couponCount;

    /**
     * 优惠券领取数量
     */
    private Integer couponReceivedCount;

    /**
     * 优惠券领取地址链接
     */
    private String activeUrl;

    /**
     * 推送类型 1-定时发送，2-实时发送
     */
    private Integer pushType;

    /**
     * 1：短信，2：app消息，3：邮箱
     */
    private Integer informType;

    /**
     * 选人条件
     */
    private MembershipFilterDTO membershipFilterDTO;

    /**
     * 定时发送消息任务开始时间
     */
    private Date pushStartTime;

    /**
     * 定时发送任务结束时间
     */
    private Date pushEndTime;

    /**
     * 每个发送周期内的发送次数，以此为依据发送消息
     */
    private Integer sendPeriodCount;

    /**
     * 活动创建/修改人
     */
    private Integer createUser;

    @Tolerate
    public SendCouponRequest() {

    }
}
