package com.ruyuan.eshop.promotion.domain.dto;

import lombok.Data;

import java.util.Date;

/**
 * @author zhonghuashishan
 */
@Data
public class SalesPromotionDTO {
    private Long id;

    private String name;

    private Date startTime;

    private Date endTime;

    private String remark;

    private Byte status;

    private Byte type;

    private String rule;

    private Integer createUser;

    private Date createTime;

    private Integer updateUser;

    private Date updateTime;
}