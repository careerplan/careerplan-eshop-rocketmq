package com.ruyuan.eshop.promotion.api;

import com.ruyuan.eshop.common.core.JsonResult;
import com.ruyuan.eshop.promotion.domain.dto.ReceiveCouponDTO;
import com.ruyuan.eshop.promotion.domain.dto.SaveOrUpdateCouponDTO;
import com.ruyuan.eshop.promotion.domain.dto.SendCouponDTO;
import com.ruyuan.eshop.promotion.domain.request.ReceiveCouponRequest;
import com.ruyuan.eshop.promotion.domain.request.SaveOrUpdateCouponRequest;
import com.ruyuan.eshop.promotion.domain.request.SendCouponRequest;

/**
 * 优惠券发放接口
 *
 * @author zhonghuashishan
 */
public interface PromotionCouponApi {

    /**
     * 新增/修改优惠券
     * @param saveOrUpdateCouponRequest
     * @return
     */
    JsonResult<SaveOrUpdateCouponDTO> saveOrUpdateCoupon(SaveOrUpdateCouponRequest saveOrUpdateCouponRequest);

    /**
     * 领取优惠券
     * @param receiveCouponRequest
     * @return
     */
    JsonResult<ReceiveCouponDTO> receiveCoupon(ReceiveCouponRequest receiveCouponRequest);

    /**
     * 领取当前正在活动中的优惠券
     * @param account 用户id
     * @return
     */
    JsonResult<Boolean> receiveCouponAvailable(Long account);

    /**
     * 发放优惠券 发放过程可以直接发送至MQ，异步消费至优惠券item表
     * @param sendCouponRequest
     * @return
     */
    JsonResult<SendCouponDTO> sendCoupon(SendCouponRequest sendCouponRequest);

    /**
     * 根据选人条件发放唤回优惠券，
     * 此接口需要调用推送系统的按条件推送消息的接口，
     * 传入选人条件，以及把优惠券+优惠券领取地址封装到一个对象中
     *
     * @param sendCouponRequest
     * @return
     */
    JsonResult<SendCouponDTO> sendCouponByConditions(SendCouponRequest sendCouponRequest);

}

