package com.ruyuan.eshop.promotion.domain.dto;

import lombok.Data;

import java.util.Date;

/**
 * @author zhonghuashishan
 */
@Data
public class SalesPromotionCouponDTO {
    private Long id;

    private String couponName;

    private Byte couponType;

    private String couponRule;

    private Date activityStartTime;

    private Date activityEndTime;

    private Long couponCount;

    private Long couponReceivedCount;

    private Byte couponReceiveType;

    private Byte couponStatus;

    private Integer createUser;

    private Date createTime;

    private Integer updateUser;

    private Date updateTime;
}