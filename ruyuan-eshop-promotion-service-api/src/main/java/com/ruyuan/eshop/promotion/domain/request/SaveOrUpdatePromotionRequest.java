package com.ruyuan.eshop.promotion.domain.request;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * 创建或更新促销活动
 *
 * @author zhonghuashishan
 */
@Data
public class SaveOrUpdatePromotionRequest implements Serializable {
    /**
     * 促销活动名称
     */
    private String name;

    /**
     * 1：短信，2：app消息，3：邮箱
     */
    private Integer informType;

    /**
     * 活动开始时间
     */
    private Date startTime;

    /**
     * 活动结束时间
     */
    private Date endTime;

    /**
     * 促销活动说明备注
     */
    private String remark;

    /**
     * 活动状态：1启用，2停用
     */
    private Integer status;

    /**
     * 活动类型：1满减，2折扣，3优惠券，4会员积分
     */
    private Integer type;

    /**
     * 活动规则
     */
    private PromotionRulesValue rule;
    /**
     * 活动规则
     */
    @Data
    public static class PromotionRulesValue implements Serializable {
        /**
         * 规则名，1：满减，2：折扣，3：优惠券，4：会员积分
         */
        private String key;
        /**
         * 规则值，其中key为条件，value为活动规则值。
         * 例如：满减规则，200，30，代表满500，减30
         * 折扣规则，200，0.95，代表满500，打0.95折
         * 优惠券，500，10，代表满500，送一张10元优惠券
         * 会员积分，1000，100，代表满1000，额外送100积分
         */
        private Map<String,String> value;
    }

    /**
     * 活动创建/修改人
     */
    private Integer createUser;
}
